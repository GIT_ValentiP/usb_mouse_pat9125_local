//
#ifndef PAT9125_H
#define PAT9125_H

#include "globals.h"
//
#define TIMEOUT_BYTE_SPI2       1
#define PAT9125_ADDR            0x75
// PAT9125 Registers
#define PAT9125_PID1            0x00
#define PAT9125_PID2            0x01
#define PAT9125_MOTION          0x02
#define PAT9125_DELTA_XL        0x03//
#define PAT9125_DELTA_YL        0x04//
#define PAT9125_MODE            0x05
#define PAT9125_CONFIG          0x06
#define PAT9125_WP              0x09
#define PAT9125_SLEEP1          0x0a
#define PAT9125_SLEEP2          0x0b
#define PAT9125_RES_X           0x0d
#define PAT9125_RES_Y           0x0e
#define PAT9125_DELTA_XYH       0x12
#define PAT9125_SHUTTER         0x14
#define PAT9125_FRAME           0x17
#define PAT9125_ORIENTATION     0x19




#define PAT9125_BANK_SELECTION  0x7f
//


    void pat9125_init();
    void pat9125_reset();
    void pat9125_set_res(uint8_t xres, uint8_t yres,bool bitres12);
    void pat9125_set_res_x(uint8_t xres);
    void pat9125_set_res_y(uint8_t yres);
    bool pat9125_read_pid(void);
    void pat9125_update();
    void pat9125_update_x2();
    void pat9125_update_x();
    void pat9125_update_y2();
    void pat9125_update_y();
    void pat9125_serial_echo();
    
    bool pat9125_read_test();
    void pat9125_grabData(void);
    void pat9125_printData(void);

#endif
//
// END OF FILE
//
