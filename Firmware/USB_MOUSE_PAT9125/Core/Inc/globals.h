/*
 * globals.h
 *
 *  Created on: 07 ago 2019
 *      Author: VALENTI
 */

#ifndef INC_GLOBALS_H_
#define INC_GLOBALS_H_

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "main.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */
#define FW_VERSION       0
#define FW_SUBVERSION    3


/** Sensor types */
typedef enum {
  SENSOR_TYPE_ACCELEROMETER = (1), /**< Gravity + linear acceleration */
  SENSOR_TYPE_MAGNETIC_FIELD = (2),
  SENSOR_TYPE_ORIENTATION = (3),
  SENSOR_TYPE_GYROSCOPE = (4),
  SENSOR_TYPE_LIGHT = (5),
  SENSOR_TYPE_PRESSURE = (6),
  SENSOR_TYPE_PROXIMITY = (8),
  SENSOR_TYPE_GRAVITY = (9),
  SENSOR_TYPE_LINEAR_ACCELERATION = (10), /**< Acceleration not including gravity */
  SENSOR_TYPE_ROTATION_VECTOR = (11),
  SENSOR_TYPE_RELATIVE_HUMIDITY = (12),
  SENSOR_TYPE_AMBIENT_TEMPERATURE = (13),
  SENSOR_TYPE_OBJECT_TEMPERATURE = (14),
  SENSOR_TYPE_VOLTAGE = (15),
  SENSOR_TYPE_CURRENT = (16),
  SENSOR_TYPE_COLOR = (17)
} sensors_type_t;

/** struct sensors_vec_s is used to return a vector in a common format. */
typedef struct {
  union {
    float v[3]; ///< 3D vector elements
    struct {
      float x; ///< X component of vector
      float y; ///< Y component of vector
      float z; ///< Z component of vector
    };         ///< Struct for holding XYZ component
    /* Orientation sensors */
    struct {
      float roll; /**< Rotation around the longitudinal axis (the plane body, 'X
                     axis'). Roll is positive and increasing when moving
                     downward. -90 degrees <= roll <= 90 degrees */
      float pitch;   /**< Rotation around the lateral axis (the wing span, 'Y
                        axis'). Pitch is positive and increasing when moving
                        upwards. -180 degrees <= pitch <= 180 degrees) */
      float heading; /**< Angle between the longitudinal axis (the plane body)
                        and magnetic north, measured clockwise when viewing from
                        the top of the device. 0-359 degrees */
    };               ///< Struct for holding roll/pitch/heading
  };                 ///< Union that can hold 3D vector array, XYZ components or
                     ///< roll/pitch/heading
  int8_t status;     ///< Status byte
  uint8_t reserved[3]; ///< Reserved
} sensors_vec_t;

/** struct sensors_color_s is used to return color data in a common format. */
typedef struct {
  union {
    float c[3]; ///< Raw 3-element data
    /* RGB color space */
    struct {
      float r;   /**< Red component */
      float g;   /**< Green component */
      float b;   /**< Blue component */
    };           ///< RGB data in floating point notation
  };             ///< Union of various ways to describe RGB colorspace
  uint32_t rgba; /**< 24-bit RGBA value */
} sensors_color_t;

/* Sensor event (36 bytes) */
/** struct sensor_event_s is used to provide a single sensor event in a common
 * format. */
typedef struct {
  int32_t version;   /**< must be sizeof(struct sensors_event_t) */
  int32_t sensor_id; /**< unique sensor identifier */
  int32_t type;      /**< sensor type */
  int32_t reserved0; /**< reserved */
  int32_t timestamp; /**< time is in milliseconds */
  union {
    float data[4];              ///< Raw data
    sensors_vec_t acceleration; /**< acceleration values are in meter per second
                                   per second (m/s^2) */
    sensors_vec_t
        magnetic; /**< magnetic vector values are in micro-Tesla (uT) */
    sensors_vec_t orientation; /**< orientation values are in degrees */
    sensors_vec_t gyro;        /**< gyroscope values are in rad/s */
    float temperature; /**< temperature is in degrees centigrade (Celsius) */
    float distance;    /**< distance in centimeters */
    float light;       /**< light in SI lux units */
    float pressure;    /**< pressure in hectopascal (hPa) */
    float relative_humidity; /**< relative humidity in percent */
    float current;           /**< current in milliamps (mA) */
    float voltage;           /**< voltage in volts (V) */
    sensors_color_t color;   /**< color in RGB component values */
  };                         ///< Union for the wide ranges of data we can carry
} sensors_event_t;

/* Sensor details (40 bytes) */
/** struct sensor_s is used to describe basic information about a specific
 * * sensor. */
typedef struct {
  char name[12];     /**< sensor name */
  int32_t version;   /**< version of the hardware + driver */
  int32_t sensor_id; /**< unique sensor identifier */
  int32_t type;      /**< this sensor's type (ex. SENSOR_TYPE_LIGHT) */
  float max_value;   /**< maximum value of this sensor's value in SI units */
  float min_value;   /**< minimum value of this sensor's value in SI units */
  float resolution; /**< smallest difference between two values reported by this
                       sensor */
  int32_t min_delay; /**< min delay in microseconds between events. zero = not a
                        constant rate */
} sensor_t;



// types
typedef struct system_timer_s
{
	double tick_1ms;              //
	double tick_10ms;              //
    double tick_100ms;            //
    double seconds;               //
    double mins;                  //
    double hours;
    bool flag_1min;//
    bool flag_10sec;
    bool flag_5sec;
    bool flag_1sec;
    bool flag_100ms;
    bool flag_10ms;
} sys_timer_t;


typedef union{
	struct int_bit_s{
			uint16_t sw1_l:1;
			uint16_t sw1_c:1;
			uint16_t sw1_r:1;
			uint16_t sw2_l:1;
			uint16_t sw2_c:1;
			uint16_t sw2_r:1;
			uint16_t sw3_l:1;
			uint16_t sw3_c:1;
			uint16_t sw3_r:1;
			uint16_t sw4_l:1;
			uint16_t sw4_c:1;
			uint16_t sw4_r:1;
			uint16_t sw5_l:1;
			uint16_t sw5_c:1;
			uint16_t sw5_r:1;
			uint16_t sw_free1:1;
	}sw;

	    uint16_t bitmap;
}Keys_Func_t;

typedef union{
	struct {
			uint16_t prog:1;
			uint16_t start_stop:1;
			uint16_t manual:1;
			uint16_t proxy_int:1;
			uint16_t acc_int:1;
			uint16_t docking_in:1;
			uint16_t detach_docking:1;
			uint16_t pwroff_sysup:1;
			uint16_t bat_refresh:1;
			uint16_t left_mouse_btn:1;
			uint16_t central_mouse_btn:1;
			uint16_t right_mouse_btn:1;
			uint16_t mouse_activity:1;
			uint16_t mouse_click:1;
			uint16_t btn_15:1;
			uint16_t btn_16:1;
	}btn;

	    uint16_t wrd;
}Btn_Func_t;



typedef union{
	struct byte_bit_s{
			uint8_t b0:1;
			uint8_t b1:1;
			uint8_t b2:1;
			uint8_t b3:1;
			uint8_t b4:1;
			uint8_t b5:1;
			uint8_t b6:1;
			uint8_t b7:1;
	}bit;
	    uint8_t bytes;
}byte_bit_t;

typedef struct {
			uint16_t duty_RED;
			uint16_t duty_GREEN;
			uint16_t duty_BLUE;
			uint8_t id_col;
}RGB_LED_t;

typedef struct {
			int step_number ;    // which step the motor is on
			int direction;      // motor direction // Direction of rotation
			unsigned long last_step_time ; // time stamp in us of the last step taken
			int number_of_steps ; // total number of steps for this motor// total number of steps this motor can take
            unsigned long step_delay;// delay between steps, in ms, based on speed
			  // Arduino pins for the motor control connection:
			int motor_pin_1;
			int motor_pin_2;
			int motor_pin_3;
			int motor_pin_4;
			int motor_pin_5;
			  // pin_count is used by the stepMotor() method:
			int pin_count;// how many pins are in use.
			int rotate;
			int speed;//in rpm
}STEPPER_t;

typedef enum
{
	STANDBY_TASK=0U,
	START_CYCLE_TASK=10,
	MOVEMENT_TASK=15,
	IONIZE_PROCESS_TASK=20,
	STOP_CYCLE_TASK=30,
	ERROR_TASK=80,
}TASK_t;

typedef struct {
			int step_for_zone ;    // how many step to move for each active zone
			int number_cycle;      // cycle in one direction -more cycle more ionizing power -
			unsigned int total_cycle;
			unsigned int total_ionize_period;
			unsigned int program_toggle_time;
			int direction;         // motor direction //
			unsigned int ionize_time ; // time applying for ionize active zone //in second
			int number_of_steps ; // total number of steps for this motor// total number of steps this motor can take
            unsigned long timeout_stop;//
            TASK_t task;
            byte_bit_t error;
}IONIZER_t;

typedef enum
{
  VLOAD_ADC    =  0U,
  VBAT_ADC     =  1U,
  VCHARGE_ADC  =  2U,
  VDD_VBAT_ADC =  3U,
  TEMP_SENS_ADC=  4U,
}ADC_Channel_t;

typedef enum
{
  LED_OFF = 0U,
  LED_ON =  1U
}LED_status_t;

typedef enum
{
  BTN_NEG_PRESSED  = 1U,
  BTN_NEG_RELEASED =  0U
}BUTTON_NEG_status_t;

typedef enum
{
  BTN_POS_PRESSED  =  1U,
  BTN_POS_RELEASED =  0U
}BUTTON_POS_status_t;

typedef enum
{
	LED_START_STOP=0U,
	LED_PROGRAM=1U,
	LED_MANUAL=2U,
}LED_RGB_type_t;

typedef enum
{
	LED_STATUS =0U,
	LED_POWER=1,
	LED_SENSOR_HALL=2,
	POWER_ON_OFF=3,
	LED_UV=4,
	STEP_UP_CTRL=5,
}LED_ID_t;

typedef union{
	struct {
			uint8_t fade:1;
			uint8_t blink:1;
			uint8_t refresh:1;
			uint8_t up_down:1;
			uint8_t f5:1;
			uint8_t f6:1;
			uint8_t f7:1;
			uint8_t f8:1;
	}f;

	    uint8_t all;
}led_func_t;

typedef struct
{
  uint16_t    duty;
  uint16_t    red;
  uint16_t    green;
  uint16_t    blue;
  uint16_t    channel[3];
  uint16_t    period;
  led_func_t  config;
  uint16_t    duty_fading;
  uint16_t    period_fading;//100ms unit

}LED_RGB_t;


typedef struct
{
    float XAxis;
    float YAxis;
    float ZAxis;
}Vector;

typedef struct{
	    Vector r;//raw data
		Vector n;//data info normalized
		Vector d;//degree
		Vector t;//temperature
		Vector old;//dps old
		// Timers
		uint32_t timer;
		float timeStep;

		// Pitch, Roll and Yaw values
		float pitch ;
		float roll  ;
		float yaw   ;

		float pitch_th ;
		float roll_th  ;
		float yaw_th   ;

		bool useCalibrate;
		float actualThreshold;
		float dpsPerDigit;
		float thresholdX;
		float thresholdY;
		float thresholdZ;
        float delta;
		uint8_t temperature;//[�C] Celsius degree
}gyroscope_t;


/**
 * @brief  Used with register 0x31 (ADXL345_REG_DATA_FORMAT) to set g range
 *
 */
typedef enum {
  ADXL345_RANGE_16_G = 0b11, ///< +/- 16g
  ADXL345_RANGE_8_G  = 0b10,  ///< +/- 8g
  ADXL345_RANGE_4_G  = 0b01,  ///< +/- 4g
  ADXL345_RANGE_2_G  = 0b00   ///< +/- 2g (default value)
} range_t;

typedef struct
{
    bool isOverrun;
    bool isWatermark;
    bool isFreeFall;
    bool isInactivity;
    bool isActivity;
    bool isActivityOnX;
    bool isActivityOnY;
    bool isActivityOnZ;
    bool isDoubleTap;
    bool isTap;
    bool isTapOnX;
    bool isTapOnY;
    bool isTapOnZ;
    bool isDataReady;
}activites_t;

typedef struct{
	  int32_t sensorID;
	  range_t range;
	  //uint8_t _clk, _do, _di, _cs;
	  bool i2c;
	  int8_t i2caddr;
	  uint8_t version ;

	  sensors_type_t type ;//= SENSOR_TYPE_ACCELEROMETER;
	  uint32_t timestamp ;
	  float acc_x ;//
	  float acc_y ;//
	  float acc_z ;//

	  float roll;
	  float pitch;
	  float yaw;

	  float roll_th;
	  float pitch_th;
	  float yaw_th;
	  float acc_th;
	  float acc_x_old;
	  float acc_z_old;

	  float froll;
	  float fpitch;
	  Vector r;
	  Vector n;
	  Vector f;

	  activites_t act;
	  char name[12] ;

	  uint32_t min_delay ;
	  float max_value    ; /* -16g = 156.9064 m/s^2  */
	  float min_value    ;  /*  16g = 156.9064 m/s^2  */
	  float resolution   ;  /*  4mg = 0.0392266 m/s^2 */
}accelerometer_t;


typedef  struct {
    uint8_t addr;
    uint8_t PID1; //product ident
    uint8_t PID2; //product ident

    uint8_t xres;
    uint8_t yres;
    //internal
    uint8_t ucMotion;
    uint8_t ucXL;
    uint8_t ucYL;
    uint8_t ucXH;
    uint8_t ucYH;
    uint8_t ucXYH;
    uint8_t iq;
    uint8_t ld_scr;//laser current
    uint8_t mfio;
    int8_t dy;
    int8_t dx;
    int16_t iDX;
    int16_t iDY;
    uint8_t gain;
    uint16_t counts;
    //bool _inverse = false;
    bool bitres12 ;//= false;

    long x ;
    long y ;
    long x2 ;
    long y2 ;
    int8_t b ;//Brightness
    uint8_t s ;//Shutter
    int8_t deltaX_low;
    int8_t deltaY_low;            //Stores the low-bits of movement data.
    int16_t deltaX_high;
	int16_t deltaY_high;          //Stores the high-bits of movement data.
    int16_t deltaX;
    int16_t deltaY;               //Stores the combined value of low and high bits.
    int16_t totalX;
    int16_t totalY;//= 0;         //Stores the total deltaX and deltaY moved during runtime.

}PAT91XX_t;


typedef enum {
  NO_SENSOR        = 0, //
  PAT9125_SENSOR   = 1,  //
  PAT9130_SENSOR   = 2,  //
} OP_SENS_TYPE_t;


/* INPUT report - this structure will represent the only INPUT report in the HID
 * descriptor.
 */
typedef struct __attribute__((packed))
{
    /* The first INPUT item is the following:
     *   0x05, 0x09,    //Usage Page (Buttons)
     *   0x19, 0x01,    //Usage Minimum (01)
     *   0x29, 0x03,    //Usage Maximum (03)
     *   0x15, 0x00,    //Logical Minimum (0)
     *   0x25, 0x01,    //Logical Maximum (1)
     *   0x95, 0x03,    //Report Count (3)
     *   0x75, 0x01,    //Report Size (1)
     *   0x81, 0x02,    //Input (Data, Variable, Absolute)
     *
     * The usage page is buttons
     * The report size is 1 (1-bit)
     * The report count is 3, thus 3 1-bit items
     * The Usage Min is 1 and the Usage maximum is 3, thus buttons 1-3, also
     *   call the primary, secondary, and tertiary buttons.
     *
     * The second INPUT item comes from the fact that the report must be byte
     * aligned, so we need to pad the previous 3-bit report with 5-bits of
     * constant(filler) data.
     *   0x95, 0x01,    //Report Count (1)
     *   0x75, 0x05,    //Report Size (5)
     *   0x81, 0x01,    //Input (Constant)
     */
    union __attribute__((packed))
    {
        struct __attribute__((packed))
        {
            unsigned button1   :1; //button LEFT
            unsigned button2   :1; //Button CENTRAL
            unsigned button3   :1;//butto RIGHT
            unsigned :5;
        };
        struct __attribute__((packed))
        {
            unsigned left   :1;
            unsigned central :1;
            unsigned right  :1;
            unsigned :5;
        };
        uint8_t value;
    } buttons;

    /* The final INPUT item is the following:
     *   0x05, 0x01,    //Usage Page (Generic Desktop)
     *   0x09, 0x30,    //Usage (X)
     *   0x09, 0x31,    //Usage (Y)
     *   0x15, 0x81,    //Logical Minimum (-127)
     *   0x25, 0x7F,    //Logical Maximum (127)
     *   0x75, 0x08,    //Report Size (8)
     *   0x95, 0x02,    //Report Count (2)
     *   0x81, 0x06,    //Input (Data, Variable, Relative)
     *
     * The report size is 8 (8-bit)
     * The report count is 2, thus 2 bytes of data.
     * The first usage is (X) and the second is (Y) so the first byte will
     *   represent the X mouse value, and the second the Y value.
     * The logical min/max determines the bounds for X and Y, -127 to 127.
     * The INPUT type is relative so each report item is relative to the last
     *   report item.  So reporting "-1" for X means that since the last report
     *   was sent, the mouse has move left
     */
    int8_t x;
    int8_t y;
    uint8_t wheel;
    //uint8_t motion_wakeup;
    union __attribute__((packed))
       {
           struct __attribute__((packed))
           {
               unsigned wakeup  :1; //motion wakeup
               unsigned :7;
           };
           uint8_t value;
       } motion;
} MOUSE_REPORT_t;


typedef enum{
USB_MOUSE_BTN=0,
USB_MOUSE_X=1,
USB_MOUSE_Y=2,
USB_MOUSE_WHEEL=3,
USB_MOUSE_WAKEUP=4,
}USB_MOUSE_REP_POS;

/* USER CODE END ET */

/* Constants */
#define SENSORS_GRAVITY_EARTH (9.80665F) /**< Earth's gravity in m/s^2 */
#define SENSORS_GRAVITY_MOON (1.6F)      /**< The moon's gravity in m/s^2 */
#define SENSORS_GRAVITY_SUN (275.0F)     /**< The sun's gravity in m/s^2 */
#define SENSORS_GRAVITY_STANDARD (SENSORS_GRAVITY_EARTH)
#define SENSORS_MAGFIELD_EARTH_MAX           (60.0F) /**< Maximum magnetic field on Earth's surface */
#define SENSORS_MAGFIELD_EARTH_MIN           (30.0F) /**< Minimum magnetic field on Earth's surface */
#define SENSORS_PRESSURE_SEALEVELHPA         (1013.25F) /**< Average sea level pressure is 1013.25 hPa */
#define SENSORS_DPS_TO_RADS                  (0.017453293F) /**< Degrees/s to rad/s multiplier         */
#define SENSORS_RADS_TO_DPS                  (57.29577793F) /**< Rad/s to degrees/s  multiplier */
#define SENSORS_GAUSS_TO_MICROTESLA          (100) /**< Gauss to micro-Tesla multiplier */

/* Exported constants --------------------------------------------------------*/


#define STEPS 32                             // change this to the number of steps on your motor

#define TIME_TO_IONIZE      10                   //time in seconds to ionize single area
#define STEPS_IONIZE_AREA   4096//steps for 5cm //2048 steps to 2,5cm//1024 steps to 3cm//2048
#define STEPS_MIN_MOVEMENT  450
#define CYCLE_IONIZE_LOW       7
#define CYCLE_IONIZE_MEDIUM    5//6
#define CYCLE_IONIZE_HIGH      4//30
#define PERIOD_IONIZE_LOW       10//seconds
#define PERIOD_IONIZE_MEDIUM    30
#define PERIOD_IONIZE_HIGH     200//60
#define TOGGLE_TIME_LOW        2//100milliseconds
#define TOGGLE_TIME_MEDIUM     4
#define TOGGLE_TIME_HIGH       6
/*TIMERS CONSTANTS DEFINE*/
#define TIMER6_RELOAD_TO_10us                       65525
#define TIMER6_PRESCALER_1MHz                          48
#define TIMER_UP_RELOAD_TO_10ms                       100
#define COUNT_TO_1ms                                  100
#define COUNT_TO_100ms                              10000
#define TIMER7_RELOAD_TO_10ms                       55535
#define OSC_EXT_FREQ                             16000000

#define NUM_LED_RGB 6
#define SW1_LED_RGB 0
#define SW2_LED_RGB 1
#define SW3_LED_RGB 2
#define SW4_LED_RGB 3
#define SW5_LED_RGB 4
#define SW6_LED_RGB 5

#define LEN_RX_RS232 6
#define LEN_TX_RS232 6
#define RGB_DELTA_TRANSITION 615//15% duty //205 5% duty

#define TIME_POWER_SWITCH_OFF   20

#define NO_KEY_PRESSED          0u
#define LEFT_SINGLE_KEY_PRESSED 1u
#define RIGHT_DOUBLE_PRESSED    2u
#define KEY_LONG_PRESSED        3u
/*MODBUS Registers*/
#define PWM_CLOCK_PRESCALER     47u
#define PWM_PERIOD_DIVIDER      999u //to have 1 KHz period

#define PERIOD_FADE_RGB         1//3//100ms unit
#define FADE_MAX_LEVEL         15
#define FADE_MIN_LEVEL          1

#define RED_LEV_0       0
#define GREEN_LEV_0     0
#define BLUE_LEV_0      30

#define RED_LEV_1        0
#define GREEN_LEV_1      0
#define BLUE_LEV_1      30

#define RED_LEV_2         10
#define GREEN_LEV_2       0
#define BLUE_LEV_2       80

#define RED_LEV_3        40
#define GREEN_LEV_3       0
#define BLUE_LEV_3      100
/* Definitions of environment analog values */
  /* Value of analog reference voltage (Vref+), connected to analog voltage   */
  /* supply Vdda (unit: mV).                                                  */
  #define VDDA_APPLI                       (3300U)
/* Definitions of data related to this example */
  /* Full-scale digital value with a resolution of 12 bits (voltage range     */
  /* determined by analog voltage references Vref+ and Vref-,                 */
  /* refer to reference manual).                                              */
#define DIGITAL_SCALE_12BITS             ((uint32_t) 0xFFF)

  /* Init variable out of ADC expected conversion data range */
#define VAR_CONVERTED_DATA_INIT_VALUE    (DIGITAL_SCALE_12BITS + 1)

  /* Definition of ADCx conversions data table size */
#define ADC_CONVERTED_DATA_BUFFER_SIZE   (4)


#define USB_MOUSE_REPORT_SIZE        (5)
/* ----------------------- Static variables ---------------------------------*/

/* ADC handler declaration */
/* Variables for ADC conversion data */
uint16_t   ADCxConvertedData[ADC_CONVERTED_DATA_BUFFER_SIZE]; /* ADC group regular conversion data (array of data) */

/* Variables for ADC conversion data computation to physical values */
uint16_t ADCx_mVolt[ADC_CONVERTED_DATA_BUFFER_SIZE];        /* Value of voltage on GPIO pin (on which is mapped ADC channel) calculated from ADC conversion data (unit: mV) */
//uint16_t uhADCxConvertedData_VrefInt_mVolt;            /* Value of internal voltage reference VrefInt calculated from ADC conversion data (unit: mV) */
int16_t ADCx_Temperature_DegreeCelsius; /* Value of temperature calculated from ADC conversion data (unit: degree Celsius) */
//uint16_t uhADCxConvertedData_VrefAnalog_mVolt;         /* Value of analog reference voltage (Vref+), connected to analog voltage supply Vdda, calculated from ADC conversion data (unit: mV) */

/* Variable to report status of DMA transfer of ADC group regular conversions */
/*  0: DMA transfer is not completed                                          */
/*  1: DMA transfer is completed                                              */
/*  2: DMA transfer has not yet been started yet (initial state)              */
uint8_t adc_conversion_status; /* Variable set into DMA interruption callback */


unsigned char char_rx_to_display,led_to_change,duty_red,duty_green,duty_blue;

unsigned char TOGGLE_PIN_LED;

unsigned char read_keyboard_event;
unsigned char display_refresh_event;
unsigned char display_refresh_start_event;
unsigned int PCA9685_delay_event;
/*Clock System timer Variables*/
sys_timer_t main_clock;

unsigned char char_disp_17seg;
RGB_LED_t led_RGB[NUM_LED_RGB];
unsigned int  RGB_color_start[3];
unsigned char RGB_Transition;

unsigned int short_press_time;
unsigned int long_press_time;
unsigned int click_double_press_time;

Keys_Func_t sensor_hall;
Btn_Func_t keys;
Btn_Func_t keys_old;
//Keys_Func_t keyboard_long;

Keys_Func_t keyboard_1click;
Keys_Func_t keyboard_2click;

STEPPER_t stepper1;
IONIZER_t ionizer1;
unsigned long stepper_micros;
bool end_process;

uint16_t version_fw;
gyroscope_t L3G42xx_sens;
accelerometer_t ADXL345_sens;
LED_RGB_t rgb[3];
uint16_t refresh_power_batt_timeout;
PAT91XX_t pat9125,pat9130;
MOUSE_REPORT_t mouse;
uint8_t mouse_report[USB_MOUSE_REPORT_SIZE];
OP_SENS_TYPE_t opt_sens;
#endif /* INC_GLOBALS_H_ */
