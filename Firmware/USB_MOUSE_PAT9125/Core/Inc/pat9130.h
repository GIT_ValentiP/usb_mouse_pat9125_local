//
#ifndef PAT9130_H
#define PAT9130_H

#include "globals.h"
//
#define TIMEOUT_BYTE_SPI2       1
#define PAT9130_ADDR            0x75
// PAT9130 Registers
#define PAT9130_PID1            0x00
#define PAT9130_PID2            0x01
#define PAT9130_MOTION          0x02
#define PAT9130_DELTA_XL        0x03//
#define PAT9130_DELTA_YL        0x04//
//#define PAT9130_MODE            0x05
#define PAT9130_CONFIG          0x06
#define PAT9130_WP              0x09
//#define PAT9130_SLEEP1          0x0a
//#define PAT9130_SLEEP2          0x0b
#define PAT9130_RES_X           0x0d
#define PAT9130_RES_Y           0x0e
#define PAT9130_DELTA_XH        0x11
#define PAT9130_DELTA_YH        0x12
#define PAT9130_IQ              0x13//Image Quality
#define PAT9130_SHUTTER         0x15
#define PAT9130_FRAME           0x17
//#define PAT9130_ORIENTATION     0x19
#define PAT9130_MFIO_CONF       0x26
#define PAT9130_LD_SRC          0x51 //Programmable Current source for LASER



#define PAT9130_BANK_SELECTION  0x7f
//


    void pat9130_init();
    void pat9130_reset();
    void pat9130_set_res(uint8_t xres, uint8_t yres,bool bitres12);
    void pat9130_set_res_x(uint8_t xres);
    void pat9130_set_res_y(uint8_t yres);
    bool pat9130_read_pid(void);
    void pat9130_update();
    void pat9130_update_x2();
    void pat9130_update_x();
    void pat9130_update_y2();
    void pat9130_update_y();
    void pat9130_serial_echo();
    
    bool pat9130_read_test();
    void pat9130_grabData(void);
    void pat9130_printData(void);

#endif
//
// END OF FILE
//
