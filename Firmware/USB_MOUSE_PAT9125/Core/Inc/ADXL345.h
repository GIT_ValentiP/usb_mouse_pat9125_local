/**************************************************************************/
/*!
    @file     Adafruit_ADXL345_U.h
    @author   K. Townsend (Adafruit Industries)


    The ADXL345 is a digital accelerometer with 13-bit resolution, capable
    of measuring up to +/-16g.  This driver communicate using I2C.

    This is a library for the Adafruit ADXL345 breakout
    ----> https://www.adafruit.com/products/1231

    Adafruit invests time and resources providing this open source code,
    please support Adafruit and open-source hardware by purchasing
    products from Adafruit!

    @section license License
    BSD (see license.txt)

    @section  HISTORY

    v1.1 - Added Adafruit_Sensor library support
    v1.0 - First release
*/
/**************************************************************************/

#ifndef ADXL345_h
#define ADXL345_h

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "globals.h"

/*=========================================================================
    I2C ADDRESS/BITS
    -----------------------------------------------------------------------*/
#define ADXL345_DEFAULT_ADDRESS (0x53) ///< Assumes ALT address pin low
/*=========================================================================*/

/*=========================================================================
    REGISTERS
    -----------------------------------------------------------------------*/
#define ADXL345_REG_DEVID (0x00)        ///< Device ID
#define ADXL345_REG_THRESH_TAP (0x1D)   ///< Tap threshold
#define ADXL345_REG_OFSX (0x1E)         ///< X-axis offset
#define ADXL345_REG_OFSY (0x1F)         ///< Y-axis offset
#define ADXL345_REG_OFSZ (0x20)         ///< Z-axis offset
#define ADXL345_REG_DUR (0x21)          ///< Tap duration
#define ADXL345_REG_LATENT (0x22)       ///< Tap latency
#define ADXL345_REG_WINDOW (0x23)       ///< Tap window
#define ADXL345_REG_THRESH_ACT (0x24)   ///< Activity threshold
#define ADXL345_REG_THRESH_INACT (0x25) ///< Inactivity threshold
#define ADXL345_REG_TIME_INACT (0x26)   ///< Inactivity time
#define ADXL345_REG_ACT_INACT_CTL   (0x27) ///< Axis enable control for activity and inactivity detection
#define ADXL345_REG_THRESH_FF (0x28) ///< Free-fall threshold
#define ADXL345_REG_TIME_FF (0x29)   ///< Free-fall time
#define ADXL345_REG_TAP_AXES (0x2A)  ///< Axis control for single/double tap
#define ADXL345_REG_ACT_TAP_STATUS (0x2B) ///< Source for single/double tap
#define ADXL345_REG_BW_RATE (0x2C)        ///< Data rate and power mode control
#define ADXL345_REG_POWER_CTL (0x2D)      ///< Power-saving features control
#define ADXL345_REG_INT_ENABLE (0x2E)     ///< Interrupt enable control
#define ADXL345_REG_INT_MAP (0x2F)        ///< Interrupt mapping control
#define ADXL345_REG_INT_SOURCE (0x30)     ///< Source of interrupts
#define ADXL345_REG_DATA_FORMAT (0x31)    ///< Data format control
#define ADXL345_REG_DATAX0 (0x32)         ///< X-axis data 0
#define ADXL345_REG_DATAX1 (0x33)         ///< X-axis data 1
#define ADXL345_REG_DATAY0 (0x34)         ///< Y-axis data 0
#define ADXL345_REG_DATAY1 (0x35)         ///< Y-axis data 1
#define ADXL345_REG_DATAZ0 (0x36)         ///< Z-axis data 0
#define ADXL345_REG_DATAZ1 (0x37)         ///< Z-axis data 1
#define ADXL345_REG_FIFO_CTL (0x38)       ///< FIFO control
#define ADXL345_REG_FIFO_STATUS (0x39)    ///< FIFO status
/*=========================================================================*/

/*=========================================================================
    REGISTERS
    -----------------------------------------------------------------------*/
#define ADXL345_MG2G_MULTIPLIER (0.004) ///< 4mg per lsb
/*=========================================================================*/

/**
 * @brief Used with register 0x2C (ADXL345_REG_BW_RATE) to set bandwidth

*/
typedef enum {
  ADXL345_DATARATE_3200_HZ = 0b1111, ///< 1600Hz Bandwidth   140�A IDD
  ADXL345_DATARATE_1600_HZ = 0b1110, ///<  800Hz Bandwidth    90�A IDD
  ADXL345_DATARATE_800_HZ  = 0b1101,  ///<  400Hz Bandwidth   140�A IDD
  ADXL345_DATARATE_400_HZ  = 0b1100,  ///<  200Hz Bandwidth   140�A IDD
  ADXL345_DATARATE_200_HZ  = 0b1011,  ///<  100Hz Bandwidth   140�A IDD
  ADXL345_DATARATE_100_HZ  = 0b1010,  ///<   50Hz Bandwidth   140�A IDD
  ADXL345_DATARATE_50_HZ   = 0b1001,   ///<   25Hz Bandwidth    90�A IDD
  ADXL345_DATARATE_25_HZ   = 0b1000,   ///< 12.5Hz Bandwidth    60�A IDD
  ADXL345_DATARATE_12_5_HZ = 0b0111, ///< 6.25Hz Bandwidth    50�A IDD
  ADXL345_DATARATE_6_25HZ  = 0b0110,  ///< 3.13Hz Bandwidth    45�A IDD
  ADXL345_DATARATE_3_13_HZ = 0b0101, ///< 1.56Hz Bandwidth    40�A IDD
  ADXL345_DATARATE_1_56_HZ = 0b0100, ///< 0.78Hz Bandwidth    34�A IDD
  ADXL345_DATARATE_0_78_HZ = 0b0011, ///< 0.39Hz Bandwidth    23�A IDD
  ADXL345_DATARATE_0_39_HZ = 0b0010, ///< 0.20Hz Bandwidth    23�A IDD
  ADXL345_DATARATE_0_20_HZ = 0b0001, ///< 0.10Hz Bandwidth    23�A IDD
  ADXL345_DATARATE_0_10_HZ = 0b0000 ///< 0.05Hz Bandwidth    23�A IDD (default value)
} dataRate_t;



typedef enum
{
    ADXL345_INT2 = 0b01,
    ADXL345_INT1 = 0b00
} adxl345_int_t;

typedef enum
{
    ADXL345_DATA_READY         = 0x07,
    ADXL345_SINGLE_TAP         = 0x06,
    ADXL345_DOUBLE_TAP         = 0x05,
    ADXL345_ACTIVITY           = 0x04,
    ADXL345_INACTIVITY         = 0x03,
    ADXL345_FREE_FALL          = 0x02,
    ADXL345_WATERMARK          = 0x01,
    ADXL345_OVERRUN            = 0x00
} adxl345_activity_t;

/**
 * @brief Class to interact with the ADXL345 accelerometer
 *
 */

  void ADXL345_Init(void);
//  Adafruit_ADXL345_Unified(uint8_t clock, uint8_t miso, uint8_t mosi,
//                           uint8_t cs, int32_t sensorID = -1);

  bool ADXL345_begin(uint8_t addr);
  void ADXL345_setRange(range_t range);
  range_t ADXL345_getRange(void);
  void ADXL345_setDataRate(dataRate_t dataRate);
  dataRate_t ADXL345_getDataRate(void);
  bool ADXL345_getEvent(void);
  void ADXL345_getSensor(void);

  uint8_t ADXL345_getDeviceID(void);
  void ADXL345_writeRegister(uint8_t reg, uint8_t value);
  uint8_t ADXL345_readRegister(uint8_t reg);
  int16_t ADXL345_read16(uint8_t reg);
  void ADXL345_writeRegisterBit(uint8_t reg, uint8_t pos, bool state);
  bool ADXL345_readRegisterBit(uint8_t reg, uint8_t pos);

  int16_t ADXL345_getX(void);
  int16_t ADXL345_getY(void);
  int16_t ADXL345_getZ(void);
  void ADXL345_clearSettings(void);

	Vector ADXL345_readRaw(void);
	Vector ADXL345_readNormalize(float gravityFactor);
	Vector ADXL345_readScaled(void);

	activites_t ADXL345_readActivites(void);

	Vector ADXL345_lowPassFilter(Vector vector, float alpha);

	void ADXL345_setTapThreshold(float threshold);
	float ADXL345_getTapThreshold(void);

	void ADXL345_setTapDuration(float duration);
	float ADXL345_getTapDuration(void);

	void ADXL345_setDoubleTapLatency(float latency);
	float ADXL345_getDoubleTapLatency(void);

	void setDoubleTapWindow(float window);
	float getDoubleTapWindow(void);

	void ADXL345_setActivityThreshold(float threshold);
	float ADXL345_getActivityThreshold(void);

	void ADXL345_setInactivityThreshold(float threshold);
	float ADXL345_getInactivityThreshold(void);

	void ADXL345_setTimeInactivity(uint8_t time);
	uint8_t ADXL345_getTimeInactivity(void);

	void ADXL345_setFreeFallThreshold(float threshold);
	float ADXL345_getFreeFallThreshold(void);

	void  ADXL345_setFreeFallDuration(float duration);
	float ADXL345_getFreeFallDuration();

	void ADXL345_setActivityX(bool state);
	bool ADXL345_getActivityX(void);
	void ADXL345_setActivityY(bool state);
	bool ADXL345_getActivityY(void);
	void ADXL345_setActivityZ(bool state);
	bool ADXL345_getActivityZ(void);
	void ADXL345_setActivityXYZ(bool state);

	void ADXL345_setInactivityX(bool state);
	bool ADXL345_getInactivityX(void);
	void ADXL345_setInactivityY(bool state);
	bool ADXL345_getInactivityY(void);
	void ADXL345_setInactivityZ(bool state);
	bool ADXL345_getInactivityZ(void);
	void ADXL345_setInactivityXYZ(bool state);

	void ADXL345_setTapDetectionX(bool state);
	bool ADXL345_getTapDetectionX(void);
	void ADXL345_setTapDetectionY(bool state);
	bool ADXL345_getTapDetectionY(void);
	void ADXL345_setTapDetectionZ(bool state);
	bool ADXL345_getTapDetectionZ(void);
	void ADXL345_setTapDetectionXYZ(bool state);

	void ADXL345_useInterrupt(adxl345_int_t interrupt);

	bool ADXL345_check_threshold(void);
  //inline uint8_t i2cread(void);
  //inline void i2cwrite(uint8_t x);

  void ADXL345_displaySensorDetails(void);
  void ADXL345_displayDataRate(void);
  void ADXL345_displayRange(void);
#endif // Adafruit_ADXL345_h
