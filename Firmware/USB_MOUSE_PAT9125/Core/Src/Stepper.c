/*
 * Stepper.cpp - Stepper library for Wiring/Arduino - Version 1.1.0
 *
 * Original library        (0.1)   by Tom Igoe.
 * Two-wire modifications  (0.2)   by Sebastian Gassner
 * Combination version     (0.3)   by Tom Igoe and David Mellis
 * Bug fix for four-wire   (0.4)   by Tom Igoe, bug fix from Noah Shibley
 * High-speed stepping mod         by Eugene Kozlenko
 * Timer rollover fix              by Eugene Kozlenko
 * Five phase five wire    (1.1.0) by Ryan Orendorff
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *
 * Drives a unipolar, bipolar, or five phase stepper motor.
 *
 * When wiring multiple stepper motors to a microcontroller, you quickly run
 * out of output pins, with each motor requiring 4 connections.
 *
 * By making use of the fact that at any time two of the four motor coils are
 * the inverse of the other two, the number of control connections can be
 * reduced from 4 to 2 for the unipolar and bipolar motors.
 *
 * A slightly modified circuit around a Darlington transistor array or an
 * L293 H-bridge connects to only 2 microcontroler pins, inverts the signals
 * received, and delivers the 4 (2 plus 2 inverted ones) output signals
 * required for driving a stepper motor. Similarly the Arduino motor shields
 * 2 direction pins may be used.
 *
 * The sequence of control signals for 5 phase, 5 control wires is as follows:
 *
 * Step C0 C1 C2 C3 C4
 *    1  0  1  1  0  1
 *    2  0  1  0  0  1
 *    3  0  1  0  1  1
 *    4  0  1  0  1  0
 *    5  1  1  0  1  0
 *    6  1  0  0  1  0
 *    7  1  0  1  1  0
 *    8  1  0  1  0  0
 *    9  1  0  1  0  1
 *   10  0  0  1  0  1
 *
 * The sequence of control signals for 4 control wires is as follows:
 *
 * Step C0 C1 C2 C3
 *    1  1  0  1  0
 *    2  0  1  1  0
 *    3  0  1  0  1
 *    4  1  0  0  1
 *
 * The sequence of controls signals for 2 control wires is as follows
 * (columns C1 and C2 from above):
 *
 * Step C0 C1
 *    1  0  1
 *    2  1  1
 *    3  1  0
 *    4  0  0
 *
 * The circuits can be found at
 *
 * http://www.arduino.cc/en/Tutorial/Stepper
 */


#include "globals.h"

#include "Stepper.h"
#include "gpio.h"

const int pole1[] ={0,0,0,0, 0,1,1,1, 0};//pole1, 8 step values
const int pole2[] ={0,0,0,1, 1,1,0,0, 0};//pole2, 8 step values
const int pole3[] ={0,1,1,1, 0,0,0,0, 0};//pole3, 8 step values
const int pole4[] ={1,1,0,0, 0,0,0,1, 0};//pole4, 8 step values

void digitalWrite(int motor_pin, GPIO_PinState status);
void driveStepper(int c);


void digitalWrite(int motor_pin,GPIO_PinState status){
	switch(motor_pin){
	      case(1):
		          HAL_GPIO_WritePin(COIL4_BLUE_GPIO_Port,COIL4_BLUE_Pin, status);

	    		  break;
	      case(2):
		          HAL_GPIO_WritePin(COIL3_PINK_GPIO_Port,COIL3_PINK_Pin, status);
				  break;
	      case(3):

	              HAL_GPIO_WritePin(COIL2_YELLOW_GPIO_Port,COIL2_YELLOW_Pin, status);
				  break;
	      case(4):
		          HAL_GPIO_WritePin(COIL1_ORANGE_GPIO_Port,COIL1_ORANGE_Pin, status);
				  break;
	      case(5):
		          //HAL_GPIO_WritePin(COIL1_ORANGE_GPIO_Port,COIL1_ORANGE_Pin, status);
				  break;
	      default:

	    	      break;
	}
}

/*
 * two-wire constructor.
 * Sets which wires should control the motor.
 */
void Stepper2Coil(int number_of_steps, int motor_pin_1, int motor_pin_2)
{
 stepper1.step_number = 0;    // which step the motor is on
 stepper1.direction = 0;      // motor direction
 stepper1.last_step_time = 0; // time stamp in us of the last step taken
 stepper1.number_of_steps = number_of_steps; // total number of steps for this motor

  // Arduino pins for the motor control connection:
 stepper1.motor_pin_1 = motor_pin_1;
 stepper1.motor_pin_2 = motor_pin_2;

  // setup the pins on the microcontroller:
 // pinMode(this->motor_pin_1, OUTPUT);
 // pinMode(this->motor_pin_2, OUTPUT);

  // When there are only 2 pins, set the others to 0:
 stepper1.motor_pin_3 = 0;
 stepper1.motor_pin_4 = 0;
 stepper1.motor_pin_5 = 0;

  // pin_count is used by the stepMotor() method:
 stepper1.pin_count = 2;
}


/*
 *   constructor for four-pin version
 *   Sets which wires should control the motor.
 */
void Stepper4Coil(int number_of_steps, int motor_pin_1, int motor_pin_2,int motor_pin_3, int motor_pin_4)
{
 stepper1.step_number = 0;    // which step the motor is on
 stepper1.direction = 0;      // motor direction
 stepper1.last_step_time = 0; // time stamp in us of the last step taken
 stepper1.number_of_steps = number_of_steps; // total number of steps for this motor

  // Arduino pins for the motor control connection:
 stepper1.motor_pin_1 = motor_pin_1;
 stepper1.motor_pin_2 = motor_pin_2;
 stepper1.motor_pin_3 = motor_pin_3;
 stepper1.motor_pin_4 = motor_pin_4;

  // setup the pins on the microcontroller:
 // pinMode(this->motor_pin_1, OUTPUT);
 // pinMode(this->motor_pin_2, OUTPUT);
 // pinMode(this->motor_pin_3, OUTPUT);
 // pinMode(this->motor_pin_4, OUTPUT);

  // When there are 4 pins, set the others to 0:
 stepper1.motor_pin_5 = 0;

  // pin_count is used by the stepMotor() method:
 stepper1.pin_count = 4;
}

/*
 *   constructor for five phase motor with five wires
 *   Sets which wires should control the motor.
 */
void Stepper5Coil(int number_of_steps, int motor_pin_1, int motor_pin_2,int motor_pin_3, int motor_pin_4,int motor_pin_5)
{
 stepper1.step_number = 0;    // which step the motor is on
 stepper1.direction = 0;      // motor direction
 stepper1.last_step_time = 0; // time stamp in us of the last step taken
 stepper1.number_of_steps = number_of_steps; // total number of steps for this motor

  // Arduino pins for the motor control connection:
 stepper1.motor_pin_1 = motor_pin_1;
 stepper1.motor_pin_2 = motor_pin_2;
 stepper1.motor_pin_3 = motor_pin_3;
 stepper1.motor_pin_4 = motor_pin_4;
 stepper1.motor_pin_5 = motor_pin_5;

  // setup the pins on the microcontroller:
 // pinMode(stepper1.motor_pin_1, OUTPUT);
 // pinMode(stepper1.motor_pin_2, OUTPUT);
 // pinMode(stepper1.motor_pin_3, OUTPUT);
 // pinMode(stepper1.motor_pin_4, OUTPUT);
 // pinMode(stepper1.motor_pin_5, OUTPUT);

  // pin_count is used by the stepMotor() method:
 stepper1.pin_count = 5;
}

/*
 * Sets the speed in revs per minute
 */
void StepperSetSpeed(long whatSpeed)
{
 //stepper1.step_delay = 60L * 1000L * 1000L /stepper1.number_of_steps / whatSpeed;//microsecond
 stepper1.step_delay = 4;//60L * 1000L /stepper1.number_of_steps / whatSpeed;//millisecond
}

/*
 * Moves the motor steps_to_move steps.  If the number is negative,
 * the motor moves in the reverse direction.

void StepperStep(int steps_to_move)
{
  unsigned long now =0;
  int steps_left = abs(steps_to_move);  // how many steps to take

  // determine direction based on whether steps_to_mode is + or -:
  if (steps_to_move > 0) {stepper1.direction = 1; }
  if (steps_to_move < 0) {stepper1.direction = 0; }

  // decrement the number of steps, moving one step each time:
  while (steps_left > 0)
  {
     // now =stepper_micros;// micros();
     // move only if the appropriate delay has passed:
     //  if (now -stepper1.last_step_time >=stepper1.step_delay)
    if(stepper_micros==0)
    {
      // get the timeStamp of when you stepped:
      stepper1.last_step_time = now;
      // increment or decrement the step number,
      // depending on direction:
      if (stepper1.direction == 1)
      {
       stepper1.step_number++;
        if (stepper1.step_number ==stepper1.number_of_steps) {
         stepper1.step_number = 0;
        }
      }
      else
      {
        if (stepper1.step_number == 0) {
         stepper1.step_number =stepper1.number_of_steps;
        }
       stepper1.step_number--;
      }
      // decrement the steps left:
      steps_left--;

      // step the motor to step number 0, 1, ..., {3 or 10}
      if (stepper1.pin_count == 5){
    	  StepperStepMotor(stepper1.step_number % 10);
      }else{
    	  StepperStepMotor(stepper1.step_number % 4);
      }

      stepper_micros=stepper1.step_delay;//reload delay
    }
  }
}
*/

void StepperStep(int steps_to_move)
{
  //int num_phase;
  //num_phase=0;
  // determine direction based on whether steps_to_mode is + or -:
  bool stop=false;
  int steps_left = abs(steps_to_move);  // how many steps to take
  int steps_move=0;
  // determine direction based on whether steps_to_mode is + or -:
  //if (steps_to_move > 0) {stepper1.direction = 1; }
  //if (steps_to_move < 0) {stepper1.direction = 0; }

  stepper_micros=0;
  stepper1.direction =ionizer1.direction;
  stepper1.step_number=0;
  // decrement the number of steps, moving one step each time:
  while ((steps_left > 0)&&(stop==false))
  {
	  // increment or decrement the step number,
	  // depending on direction:
	  if(steps_move>STEPS_MIN_MOVEMENT){
	     stop=read_sensor();
	  }
	  if(stop==false){//not reached
		  if (stepper1.direction == 1){
		   stepper1.step_number++;
			if (stepper1.step_number >7){//stepper1.number_of_steps) {
			 stepper1.step_number = 0;
			}
		  }
		  else{
			if (stepper1.step_number == 0) {
			 stepper1.step_number =7;//stepper1.number_of_steps;
			}
			stepper1.step_number--;
		  }
		  driveStepper(stepper1.step_number);
		  // decrement the steps left:
		  steps_left--;
		  steps_move++;
		  //StepperStepMotor(stepper1.step_number);
		  //TOGGLE_PIN_LED =((TOGGLE_PIN_LED==1) ? 0:1);
		  //set_led(2,TOGGLE_PIN_LED);
		  stepper_micros=2;//stepper1.step_delay;//reload delay
		  //while(stepper_micros !=0);
		  HAL_Delay(1);
	  }

  }
  driveStepper(8);
}


/*
 * Moves the motor forward or backwards.
 */
void StepperStepMotor(int thisStep)
{
  if (stepper1.pin_count == 2) {
    switch (thisStep) {
      case 0:  // 01
        digitalWrite(stepper1.motor_pin_1, LOW);
        digitalWrite(stepper1.motor_pin_2, HIGH);
      break;
      case 1:  // 11
        digitalWrite(stepper1.motor_pin_1, HIGH);
        digitalWrite(stepper1.motor_pin_2, HIGH);
      break;
      case 2:  // 10
        digitalWrite(stepper1.motor_pin_1, HIGH);
        digitalWrite(stepper1.motor_pin_2, LOW);
      break;
      case 3:  // 00
        digitalWrite(stepper1.motor_pin_1, LOW);
        digitalWrite(stepper1.motor_pin_2, LOW);
      break;
    }
  }
  if (stepper1.pin_count == 4) {
    switch (thisStep) {
      case 0:  // 1010
        digitalWrite(stepper1.motor_pin_1, HIGH);
        digitalWrite(stepper1.motor_pin_2, LOW);
        digitalWrite(stepper1.motor_pin_3, HIGH);
        digitalWrite(stepper1.motor_pin_4, LOW);
      break;
      case 1:  // 0110
        digitalWrite(stepper1.motor_pin_1, LOW);
        digitalWrite(stepper1.motor_pin_2, HIGH);
        digitalWrite(stepper1.motor_pin_3, HIGH);
        digitalWrite(stepper1.motor_pin_4, LOW);
      break;
      case 2:  //0101
        digitalWrite(stepper1.motor_pin_1, LOW);
        digitalWrite(stepper1.motor_pin_2, HIGH);
        digitalWrite(stepper1.motor_pin_3, LOW);
        digitalWrite(stepper1.motor_pin_4, HIGH);
      break;
      case 3:  //1001
        digitalWrite(stepper1.motor_pin_1, HIGH);
        digitalWrite(stepper1.motor_pin_2, LOW);
        digitalWrite(stepper1.motor_pin_3, LOW);
        digitalWrite(stepper1.motor_pin_4, HIGH);
      break;
    }
  }

  if (stepper1.pin_count == 5) {
    switch (thisStep) {
      case 0:  // 01101
        digitalWrite(stepper1.motor_pin_1, LOW);
        digitalWrite(stepper1.motor_pin_2, HIGH);
        digitalWrite(stepper1.motor_pin_3, HIGH);
        digitalWrite(stepper1.motor_pin_4, LOW);
        digitalWrite(stepper1.motor_pin_5, HIGH);
        break;
      case 1:  // 01001
        digitalWrite(stepper1.motor_pin_1, LOW);
        digitalWrite(stepper1.motor_pin_2, HIGH);
        digitalWrite(stepper1.motor_pin_3, LOW);
        digitalWrite(stepper1.motor_pin_4, LOW);
        digitalWrite(stepper1.motor_pin_5, HIGH);
        break;
      case 2:  // 01011
        digitalWrite(stepper1.motor_pin_1, LOW);
        digitalWrite(stepper1.motor_pin_2, HIGH);
        digitalWrite(stepper1.motor_pin_3, LOW);
        digitalWrite(stepper1.motor_pin_4, HIGH);
        digitalWrite(stepper1.motor_pin_5, HIGH);
        break;
      case 3:  // 01010
        digitalWrite(stepper1.motor_pin_1, LOW);
        digitalWrite(stepper1.motor_pin_2, HIGH);
        digitalWrite(stepper1.motor_pin_3, LOW);
        digitalWrite(stepper1.motor_pin_4, HIGH);
        digitalWrite(stepper1.motor_pin_5, LOW);
        break;
      case 4:  // 11010
        digitalWrite(stepper1.motor_pin_1, HIGH);
        digitalWrite(stepper1.motor_pin_2, HIGH);
        digitalWrite(stepper1.motor_pin_3, LOW);
        digitalWrite(stepper1.motor_pin_4, HIGH);
        digitalWrite(stepper1.motor_pin_5, LOW);
        break;
      case 5:  // 10010
        digitalWrite(stepper1.motor_pin_1, HIGH);
        digitalWrite(stepper1.motor_pin_2, LOW);
        digitalWrite(stepper1.motor_pin_3, LOW);
        digitalWrite(stepper1.motor_pin_4, HIGH);
        digitalWrite(stepper1.motor_pin_5, LOW);
        break;
      case 6:  // 10110
        digitalWrite(stepper1.motor_pin_1, HIGH);
        digitalWrite(stepper1.motor_pin_2, LOW);
        digitalWrite(stepper1.motor_pin_3, HIGH);
        digitalWrite(stepper1.motor_pin_4, HIGH);
        digitalWrite(stepper1.motor_pin_5, LOW);
        break;
      case 7:  // 10100
        digitalWrite(stepper1.motor_pin_1, HIGH);
        digitalWrite(stepper1.motor_pin_2, LOW);
        digitalWrite(stepper1.motor_pin_3, HIGH);
        digitalWrite(stepper1.motor_pin_4, LOW);
        digitalWrite(stepper1.motor_pin_5, LOW);
        break;
      case 8:  // 10101
        digitalWrite(stepper1.motor_pin_1, HIGH);
        digitalWrite(stepper1.motor_pin_2, LOW);
        digitalWrite(stepper1.motor_pin_3, HIGH);
        digitalWrite(stepper1.motor_pin_4, LOW);
        digitalWrite(stepper1.motor_pin_5, HIGH);
        break;
      case 9:  // 00101
        digitalWrite(stepper1.motor_pin_1, LOW);
        digitalWrite(stepper1.motor_pin_2, LOW);
        digitalWrite(stepper1.motor_pin_3, HIGH);
        digitalWrite(stepper1.motor_pin_4, LOW);
        digitalWrite(stepper1.motor_pin_5, HIGH);
        break;
    }
  }
}

/*
  version() returns the version of the library:
*/
int StepperVersion(void)
{
  return 5;
}



/*
 * @brief sends signal to the motor
 * @param "c" is integer representing the pol of motor
 * @return does not return anything
 *
 * www.Robojax.com code June 2019
 */
void driveStepper(int c)
{
     digitalWrite(1, pole1[c]);
     digitalWrite(2, pole2[c]);
     digitalWrite(3, pole3[c]);
     digitalWrite(4, pole4[c]);
}//driveStepper end here

