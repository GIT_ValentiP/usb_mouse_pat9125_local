/**
  ******************************************************************************
  * @file    adc.c
  * @brief   This file provides code for the configuration
  *          of the ADC instances.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "adc.h"

/* USER CODE BEGIN 0 */
#include "globals.h"
#include "gpio.h"
/* ADC internal channels related definitions */
/* Internal voltage reference VrefInt */

/* Temperature sensor */

#define ADC_CFGR1_RES_BITOFFSET_POS        ( 3UL)                       /* Value equivalent to bitfield "ADC_CFGR1_RES" position in register */

#define TEMP110_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FFFF7C2))
#define TEMP30_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FFFF7B8))
#define VDD_CALIB ((uint16_t) (330))
#define VDD_APPLI ((uint16_t) (300))


/** @defgroup ADC_LL_EC_RESOLUTION  ADC instance - Resolution
  * @{
  */
#define LL_ADC_RESOLUTION_12B              (0x00000000UL)                      /*!< ADC resolution 12 bits */
#define LL_ADC_RESOLUTION_10B              (                  ADC_CFGR1_RES_0) /*!< ADC resolution 10 bits */
#define LL_ADC_RESOLUTION_8B               (ADC_CFGR1_RES_1                  ) /*!< ADC resolution  8 bits */
#define LL_ADC_RESOLUTION_6B               (ADC_CFGR1_RES_1 | ADC_CFGR1_RES_0) /*!< ADC resolution  6 bits */
/** @defgroup ADC_LL_EC_HELPER_MACRO  Definitions of constants used by helper macro
  * @{
  */
#define LL_ADC_TEMPERATURE_CALC_ERROR     ((int16_t)0x7FFF) /* Temperature calculation error using helper macro @ref __LL_ADC_CALC_TEMPERATURE(), due to issue on calibration parameters. This value is coded on 16 bits (to fit on signed word or double word) and corresponds to an inconsistent temperature value. */
/**
  * @brief  Helper macro to define the ADC conversion data full-scale digital
  *         value corresponding to the selected ADC resolution.
  * @note   ADC conversion data full-scale corresponds to voltage range
  *         determined by analog voltage references Vref+ and Vref-
  *         (refer to reference manual).
  * @param  __ADC_RESOLUTION__ This parameter can be one of the following values:
  *         @arg @ref LL_ADC_RESOLUTION_12B
  *         @arg @ref LL_ADC_RESOLUTION_10B
  *         @arg @ref LL_ADC_RESOLUTION_8B
  *         @arg @ref LL_ADC_RESOLUTION_6B
  * @retval ADC conversion data full-scale digital value (unit: digital value of ADC conversion data)
  */
#define __LL_ADC_DIGITAL_SCALE(__ADC_RESOLUTION__)                             \
  (0xFFFUL >> ((__ADC_RESOLUTION__) >> (ADC_CFGR1_RES_BITOFFSET_POS - 1UL)))

/**
  * @brief  Helper macro to convert the ADC conversion data from
  *         a resolution to another resolution.
  * @param  __DATA__ ADC conversion data to be converted
  * @param  __ADC_RESOLUTION_CURRENT__ Resolution of the data to be converted
  *         This parameter can be one of the following values:
  *         @arg @ref LL_ADC_RESOLUTION_12B
  *         @arg @ref LL_ADC_RESOLUTION_10B
  *         @arg @ref LL_ADC_RESOLUTION_8B
  *         @arg @ref LL_ADC_RESOLUTION_6B
  * @param  __ADC_RESOLUTION_TARGET__ Resolution of the data after conversion
  *         This parameter can be one of the following values:
  *         @arg @ref LL_ADC_RESOLUTION_12B
  *         @arg @ref LL_ADC_RESOLUTION_10B
  *         @arg @ref LL_ADC_RESOLUTION_8B
  *         @arg @ref LL_ADC_RESOLUTION_6B
  * @retval ADC conversion data to the requested resolution
  */


#define __LL_ADC_CALC_DATA_TO_VOLTAGE(__VREFANALOG_VOLTAGE__,\
                                      __ADC_DATA__,\
                                      __ADC_RESOLUTION__)                      \
  ((__ADC_DATA__) * (__VREFANALOG_VOLTAGE__)                                   \
   / __LL_ADC_DIGITAL_SCALE(__ADC_RESOLUTION__)                                \
  )

int32_t temperature_calc(int32_t sensorValue);

/* USER CODE END 0 */

ADC_HandleTypeDef hadc;

/* ADC init function */
void MX_ADC_Init(void)
{
  ADC_ChannelConfTypeDef sConfig = {0};

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.DiscontinuousConvMode = ENABLE;//DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel to be converted.
    */
    sConfig.Channel = ADC_CHANNEL_3;//VLOAD_ADC
    sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
    sConfig.SamplingTime =ADC_SAMPLETIME_41CYCLES_5;//ADC_SAMPLETIME_28CYCLES_5;// ADC_SAMPLETIME_1CYCLE_5;
    if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
    {
      Error_Handler();
    }
  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_12;//VBAT_ADC
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_41CYCLES_5;//ADC_SAMPLETIME_28CYCLES_5;//ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_13;//VCHARGE_ADC
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_41CYCLES_5;//ADC_SAMPLETIME_28CYCLES_5;//ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
   /** Configure Regular Channel
  */
  /* (1) Select CHSEL16 for temperature sensor */
  /* (2) Select a sampling mode of 111 i.e. 239.5 ADC clk to be greater than
  17.1us */
  /* (3) Wake-up the Temperature sensor (only for VBAT, Temp sensor and
  VRefInt) */
  sConfig.Channel = ADC_CHANNEL_VBAT;//ADC_CHANNEL_TEMPSENSOR;//V_TEMP ADC1->CHSELR = ADC_CHSELR_CHSEL16; /* (1) */
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_41CYCLES_5;//ADC_SAMPLETIME_13CYCLES_5;//ADC_SAMPLETIME_1CYCLE_5;  ADC1->SMPR |= ADC_SMPR_SMP_0 | ADC_SMPR_SMP_1 | ADC_SMPR_SMP_2; /* (2) */
  //sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {// ADC->CCR |= ADC_CCR_TSEN; /* (3) */
	  Error_Handler();
  }


//  sConfig.Channel = ADC_CHANNEL_VBAT;
//  //sConfig.Rank = ADC_REGULAR_RANK_2;
//  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
//  {
//	  Error_Handler();
//  }
}

void HAL_ADC_MspInit(ADC_HandleTypeDef* adcHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  if(adcHandle->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspInit 0 */

  /* USER CODE END ADC1_MspInit 0 */
    /* ADC1 clock enable */
    __HAL_RCC_ADC1_CLK_ENABLE();

    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**ADC GPIO Configuration
    PC2     ------> ADC_IN12
    PC3     ------> ADC_IN13
    PA3     ------> ADC_IN3
    */
    GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    GPIO_InitStruct.Pin = GPIO_PIN_3;
    GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /* USER CODE BEGIN ADC1_MspInit 1 */
    HAL_NVIC_SetPriority(ADC1_COMP_IRQn, 0, 0);
    HAL_NVIC_EnableIRQ(ADC1_COMP_IRQn);
  /* USER CODE END ADC1_MspInit 1 */
  }
}

void HAL_ADC_MspDeInit(ADC_HandleTypeDef* adcHandle)
{

  if(adcHandle->Instance==ADC1)
  {
  /* USER CODE BEGIN ADC1_MspDeInit 0 */

  /* USER CODE END ADC1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_ADC1_CLK_DISABLE();

    /**ADC GPIO Configuration
    PC2     ------> ADC_IN12
    PC3     ------> ADC_IN13
    PA3     ------> ADC_IN3
    */
    HAL_GPIO_DeInit(GPIOC, GPIO_PIN_2|GPIO_PIN_3);

    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_3);

  /* USER CODE BEGIN ADC1_MspDeInit 1 */

  /* USER CODE END ADC1_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */


int32_t temperature_calc(int32_t sensorValue){

//int32_t temperature,delta,delta2,delta3;

/* Temperature sensor calibration value address */

int32_t temperature; /* will contain the temperature in degrees Celsius */
temperature = ((sensorValue * VDD_APPLI / VDD_CALIB)- (int32_t) *TEMP30_CAL_ADDR );
temperature = temperature * (int32_t)(110 - 30);
temperature = temperature / (int32_t)(*TEMP110_CAL_ADDR- *TEMP30_CAL_ADDR);
temperature = temperature + 30;
 return(temperature);
}

void Init_ADC(){
	uint8_t id;
/* Variables for ADC conversion data computation to physical values */


ADCx_Temperature_DegreeCelsius = 0U; /* Value of temperature calculated from ADC conversion data (unit: degree Celsius) */

/* Variable to report status of DMA transfer of ADC group regular conversions */
/*  0: DMA transfer is not completed                                          */
/*  1: DMA transfer is completed                                              */
/*  2: DMA transfer has not yet been started yet (initial state)              */
adc_conversion_status = 0; /* Variable set into DMA interruption callback */

for (id = 0; id < ADC_CONVERTED_DATA_BUFFER_SIZE; id++)
{
  ADCxConvertedData[id] = VAR_CONVERTED_DATA_INIT_VALUE;
  ADCx_mVolt[id] = 0U;        /* Value of voltage on GPIO pin (on which is mapped ADC channel) calculated from ADC conversion data (unit: mV) */
}

/* Run the ADC calibration */
 if (HAL_ADCEx_Calibration_Start(&hadc) != HAL_OK)
 {
   /* Calibration Error */
    set_led(LED_STATUS, LED_ON);//	 LED_STATUS
 }

}



void Update_ADC(uint16_t ch){
	uint8_t id;


	/* Optionally, for this example purpose, calculate analog reference       */
	/* voltage (Vref+) from ADC conversion of internal voltage reference      */
	/* VrefInt.                                                               */
	/* This voltage should correspond to value of literal "VDDA_APPLI".       */
	/* Note: This calculation can be performed when value of voltage Vref+    */
	/* is unknown in the application.                                         */
	//uhADCxConvertedData_VrefAnalog_mVolt = __LL_ADC_CALC_VREFANALOG_VOLTAGE(aADCxConvertedData[1], LL_ADC_RESOLUTION_12B);
id=0;
  //ADC_CONVERTED_DATA_BUFFER_SIZE
	for (id = 0; id < ADC_CONVERTED_DATA_BUFFER_SIZE; id++)
	{
		//ADCxConvertedData[id] = VAR_CONVERTED_DATA_INIT_VALUE;
		/* Start ADC conversion */
		adc_conversion_status=0;
		if (HAL_ADC_Start_IT(&hadc) != HAL_OK){
			// Error_Handler();
			set_led(LED_STATUS, LED_ON);//	 LED_STATUS
		}
//		else{
//			ADCxConvertedData[id]=HAL_ADC_GetValue(&hadc);
//		}

		while (adc_conversion_status==0);
		ADCxConvertedData[id]=HAL_ADC_GetValue(&hadc);
	}
	HAL_ADC_Stop_IT(&hadc);
	/* Computation of ADC conversions raw data to physical values           */
	/* using LL ADC driver helper macro.                                    */
	/* Note: ADC results are transferred into array "aADCxConvertedData"  */
	/*       in the order of their rank in ADC sequencer.                   */
 	ADCx_mVolt[VLOAD_ADC]      = __LL_ADC_CALC_DATA_TO_VOLTAGE(VDDA_APPLI, ADCxConvertedData[VLOAD_ADC], LL_ADC_RESOLUTION_12B)*2;
	ADCx_mVolt[VBAT_ADC]       = __LL_ADC_CALC_DATA_TO_VOLTAGE(VDDA_APPLI, ADCxConvertedData[VBAT_ADC], LL_ADC_RESOLUTION_12B)*2;
	ADCx_mVolt[VCHARGE_ADC]    = __LL_ADC_CALC_DATA_TO_VOLTAGE(VDDA_APPLI, ADCxConvertedData[VCHARGE_ADC], LL_ADC_RESOLUTION_12B)*2;
	ADCx_mVolt[VDD_VBAT_ADC]   = __LL_ADC_CALC_DATA_TO_VOLTAGE(VDDA_APPLI, ADCxConvertedData[VDD_VBAT_ADC], LL_ADC_RESOLUTION_12B)*2;
	//uhADCxConvertedData_VrefInt_mVolt            = __LL_ADC_CALC_DATA_TO_VOLTAGE(VDDA_APPLI, aADCxConvertedData[2], LL_ADC_RESOLUTION_12B);
	//ADCx_Temperature_DegreeCelsius =temperature_calc((int32_t)ADCxConvertedData[TEMP_SENS_ADC]);// __LL_ADC_CALC_TEMPERATURE(VDDA_APPLI, ADCxConvertedData[TEMP_SENS_ADC], LL_ADC_RESOLUTION_12B);
}


/* USER CODE END 1 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
