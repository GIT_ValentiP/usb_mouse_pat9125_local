/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "can.h"
#include "i2c.h"
#include "iwdg.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbd_hid.h"
#include "globals.h"
#include "Stepper.h"                         // Include the Stepper motor header file
#ifdef L3G4200D_EN
 #include "L3G4200D.h"
#endif
#include "ADXL345.h"
#include "pat9125.h"
#include "pat9130.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#ifdef __GNUC__
  /* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
  #define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
  #define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */


#define PAT9125_EN
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
extern USBD_HandleTypeDef hUsbDeviceFS;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void task_manager(void);
void ionize_process(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_IWDG_Init();
  MX_SPI2_Init();
  MX_TIM1_Init();
  MX_TIM7_Init();
  //MX_USART2_UART_Init();
  MX_USART3_UART_Init();
  MX_ADC_Init();
  MX_TIM2_Init();
  //MX_CAN_Init();
  MX_TIM3_Init();
  MX_USB_DEVICE_Init();

  /* USER CODE BEGIN 2 */

  version_fw=((FW_VERSION<<8)| FW_SUBVERSION);

  Main_Clock_Init();

  HAL_TIM_Base_MspInit(&htim7);
  HAL_UART_MspInit(&huart3);
  HAL_I2C_MspInit(&hi2c1);
  HAL_ADC_MspInit(&hadc);
  Init_ADC();
  HAL_IWDG_Refresh(&hiwdg);

  HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_1 );
  HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_2 );
  HAL_TIM_PWM_Start(&htim1,TIM_CHANNEL_3 );

  HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_1 );
  HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_2 );
  HAL_TIM_PWM_Start(&htim3,TIM_CHANNEL_4 );

  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_1 );
  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_2 );
  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_3 );
  HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_4 );
  printf("\n\r *** FW Version: %3d.%3d ***\n\r",FW_VERSION,FW_SUBVERSION);
  stepper1.rotate = 0;
  Stepper4Coil(STEPS, 1, 3, 2, 4);  // create an instance of the stepper class using the steps and pins
                                  //declare variable rotate with 0 for input rotation.
  read_sensor();

  HAL_I2C_MspInit(&hi2c1);
  //L3G4200D_init();
#ifdef  ADXL345_EN
  ADXL345_Init();
#endif
  ionizer1.task=STANDBY_TASK;
  HAL_SPI_MspInit(&hspi2);
  if(pat9125_read_pid()==true){
	  opt_sens=PAT9125_SENSOR;
  }else if(pat9130_read_pid()==true){
	  opt_sens=PAT9130_SENSOR;
  }

if( opt_sens==PAT9125_SENSOR){
  pat9125_init();
}
if( opt_sens==PAT9130_SENSOR){
  pat9130_init();//
}

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  //set_led_rgb(1,100,0,0);
  //set_led_rgb(3,0,0,100);
  rgb[LED_START_STOP].config.all=0;
  rgb[LED_START_STOP].config.f.refresh=1;//rgb[LED_START_STOP].config.f.fade=1;
  //rgb[LED_START_STOP].config.f.up_down=0;
  //rgb[LED_START_STOP].duty_fading=FADE_MIN_LEVEL;
  rgb[LED_START_STOP].red=0;
  rgb[LED_START_STOP].green=0;
  rgb[LED_START_STOP].blue=0;

  rgb[LED_PROGRAM].config.all=0;
  rgb[LED_PROGRAM].config.f.refresh=1;
  rgb[LED_PROGRAM].red=0;
  rgb[LED_PROGRAM].green=0;
  rgb[LED_PROGRAM].blue=0;

  ADXL345_sens.acc_x_old=ADXL345_sens.acc_x;
  keys.btn.detach_docking=1;
  keys.btn.bat_refresh=1;
  while (1)
  {
	  HAL_IWDG_Refresh(&hiwdg);
	  if( opt_sens==PAT9125_SENSOR){
	    pat9125_grabData();
	  }
	  if( opt_sens==PAT9130_SENSOR){
		  pat9130_grabData();//
	  }

//	  keys.btn.bat_refresh=1;
//	  if(keys.btn.manual==1){
//		set_led(LED_UV,LED_ON);
//		keys.btn.bat_refresh=1;
//		set_led_rgb(LED_MANUAL,0,0,100);
//	  }else{
//		set_led(LED_UV,LED_OFF);
//		set_led_rgb(LED_MANUAL,0,80,0);
//	  }
	  if(main_clock.flag_10ms==1){
		 read_sensor();
		 //pat9130_grabData();//pat9125_grabData();
		 main_clock.flag_10ms=0;
	  }

	  if(main_clock.flag_100ms==1){
		    //pat9130_grabData();//pat9125_grabData();
			main_clock.flag_100ms=0;
		}

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
		if(main_clock.flag_1sec==1){
			 // Update_ADC(0);
			  //pat9125_grabData();
			  //pat9125_printData();
			  /* Display the results (acceleration is measured in m/s^2) */
#ifdef  ADXL345_EN
			  if(ADXL345_sens.sensorID!=0){
				  //printf("ACC_X:%5.1f [m/s^2]\n\r",ADXL345_sens.acc_x);
				  //printf("ACC_Y:%5.1f [m/s^2]\n\r",ADXL345_sens.acc_y);
				  printf("ACC_Z:%5.1f Old:%5.1f [m/s^2]\n\r",ADXL345_sens.acc_z,ADXL345_sens.acc_z_old);
				  printf("ACC_Pitch = %3.1f� \n\r",ADXL345_sens.pitch);
				  printf("ACC_Roll  = %3.1f� \n\r",ADXL345_sens.roll);
				  //printf("ACC_Yaw   = %3.1f� \n\r",ADXL345_sens.yaw);
			  }
#endif
			//  printf("Task:%d\n\r",ionizer1.task);
			//  printf("Prog.:%d\n\r",read_keyboard_event);
//			  printf("Btn PROGRAM:%d \n\r",keys.btn.prog);
//			  printf("Btn StartStop:%d \n\r",keys.btn.start_stop);
//			  printf("Btn MANUAL:%d\n\r",keys.btn.manual);
//			  printf("DOCKING:%d\n\r",keys.btn.docking_in);
//			  printf("SensorHALL S:%d\n\r",sensor_hall.sw.sw1_l);
//			  printf("SensorHALL D:%d\n\r",sensor_hall.sw.sw1_r);
			  //printf("VLOAD:%d mV;Curr:%d mA\n\r",ADCx_mVolt[VLOAD_ADC],(ADCx_mVolt[VLOAD_ADC]/5));
			  //printf("VBAT:%d mV\n\r",ADCx_mVolt[VBAT_ADC]);
			  //printf("VCHARGE:%d mV\n\r",ADCx_mVolt[VCHARGE_ADC]);
			  //printf("VDD at VBAT_pin:%d mV\n\r",ADCx_mVolt[VDD_VBAT_ADC]);
			  //printf("TEMP:%d�C\n\r",ADCx_Temperature_DegreeCelsius);
//			  if(refresh_power_batt_timeout>0){
//				  refresh_power_batt_timeout--;
//			  }else{
//				  if(keys.btn.bat_refresh==1){
//				     kick_power_switch(1);
//				     refresh_power_batt_timeout=6;//refresh every 20 seconds
//				     keys.btn.bat_refresh=0;
//				  }
//			  }
			  main_clock.flag_1sec=0;
		}
        if(keys.btn.mouse_activity==1){
        	mouse_report[USB_MOUSE_BTN]=mouse.buttons.value;
        	mouse_report[USB_MOUSE_X]=mouse.x;
        	mouse_report[USB_MOUSE_Y]=mouse.y;
        	mouse_report[USB_MOUSE_WHEEL]=0;
        	USBD_HID_SendReport(&hUsbDeviceFS, mouse_report, USB_MOUSE_REPORT_SIZE);// send button press
        	keys.btn.mouse_activity=0;
        }
		//refresh_led_rgb();
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14
                              |RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI48;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the EVAL_COM1 and Loop until the end of transmission */

  HAL_UART_Transmit(&huart3, (uint8_t *)&ch, 1, 0xFFFF);
 // HAL_UART_Transmit(&huart2, (uint8_t *)&ch, 1, 0xFFFF);


  return ch;
}


void task_manager(void){
	bool stop;


	switch(ionizer1.task){
		case(STANDBY_TASK):
		                   ionizer1.task=STANDBY_TASK;
		                   set_led(LED_UV,LED_OFF);
		                   set_led_rgb(LED_MANUAL,0,0,0);
						   if(display_refresh_event==1){
									//StepperSetSpeed(stepper1.speed);                 //sets the speed of motor
									//StepperStep(stepper1.rotate);                        //makes the motor to rotate
									//printf("\n\r Step:%3d Speed:%3d[rpm] \n\r",stepper1.rotate,stepper1.speed);//prints the value you specified to rotate
									//end_process=false;
									//ionize_process();

							        ionizer1.task=START_CYCLE_TASK;
							}
				            break;

		case(START_CYCLE_TASK):
		                       ionizer1.task=START_CYCLE_TASK;
		                       set_led(LED_UV,LED_OFF);
				               stop=read_sensor();
				               keys.btn.bat_refresh=1;
				               if(keys.btn.start_stop==BTN_NEG_RELEASED){//START/STOP button released
	                              ionizer1.task=MOVEMENT_TASK;
	                              ionizer1.number_cycle=0;
	                              ionizer1.direction =1;
	                              //L3G4200D_init();//calibrate gyroscope
	                              rgb[LED_START_STOP].config.all=0;
	                              rgb[LED_START_STOP].config.f.blink=1;
	                              rgb[LED_START_STOP].red=0;
	                              rgb[LED_START_STOP].green=100;
	                              rgb[LED_START_STOP].blue=0;
	                              end_process=false;
				               }
						       break;

		case(MOVEMENT_TASK):
		                    ionizer1.task=MOVEMENT_TASK;
	                    	stepper1.rotate=STEPS_IONIZE_AREA;
		    	            StepperStep(stepper1.rotate);
				            stop=read_sensor();
				            keys.btn.bat_refresh=1;
				            if(end_process==false){
								if(stop==true){
								  if(++ionizer1.number_cycle>ionizer1.total_cycle){//end cycle
									display_refresh_event=0;
									set_led(LED_UV,LED_OFF);
									ionizer1.task=STANDBY_TASK;
									rgb[LED_START_STOP].config.all=0;
									rgb[LED_START_STOP].config.f.fade=1;
									rgb[LED_START_STOP].config.f.up_down=0;
									rgb[LED_START_STOP].duty_fading=FADE_MIN_LEVEL;
								  }else{//limit switch hall sensor
									  ionizer1.ionize_time= ionizer1.total_ionize_period;//TIME_TO_IONIZE;
									  ionizer1.task=IONIZE_PROCESS_TASK;
									  rgb[LED_START_STOP].config.all=0;
									  rgb[LED_START_STOP].config.f.blink=1;//rgb[LED_START_STOP].config.f.refresh=1;
									  rgb[LED_START_STOP].red=0;
									  rgb[LED_START_STOP].green=0;
									  rgb[LED_START_STOP].blue=100;

									  rgb[LED_PROGRAM].config.all=0;
									  rgb[LED_PROGRAM].config.f.blink=1;
									  rgb[LED_PROGRAM].red=0;
									  rgb[LED_PROGRAM].green=0;
									  rgb[LED_PROGRAM].blue=100;
								  }
								  ionizer1.direction =((ionizer1.direction == 1)?0:1);
								}else{//end partial movement
								  set_led(LED_UV,LED_ON);
								  ionizer1.ionize_time= ionizer1.total_ionize_period;//TIME_TO_IONIZE;
								  ionizer1.task=IONIZE_PROCESS_TASK;
								  rgb[LED_START_STOP].config.all=0;
								  rgb[LED_START_STOP].config.f.blink=1;//rgb[LED_START_STOP].config.f.refresh=1;
								  rgb[LED_START_STOP].red=0;
								  rgb[LED_START_STOP].green=0;
								  rgb[LED_START_STOP].blue=100;

								  rgb[LED_PROGRAM].config.all=0;
								  rgb[LED_PROGRAM].config.f.blink=1;
								  rgb[LED_PROGRAM].red=0;
								  rgb[LED_PROGRAM].green=0;
								  rgb[LED_PROGRAM].blue=100;
								}
				            }else{
				            	ionizer1.number_cycle=0;
				            	set_led(LED_UV,LED_OFF);
				            	ionizer1.task=STOP_CYCLE_TASK;
				            	printf(" STOP Cycle \n\r");
				            }
						    break;

		case(IONIZE_PROCESS_TASK):
		                            ionizer1.task=IONIZE_PROCESS_TASK;
		                            stop=read_sensor();
		                            keys.btn.bat_refresh=1;
						            if(end_process==false){
										set_led(LED_UV,LED_ON);
										//ionizer1.task=IONIZE_PROCESS_TASK;
										if(ionizer1.ionize_time==0){
										   set_led(LED_UV,LED_OFF);
										   ionizer1.task=MOVEMENT_TASK;
										   rgb[LED_START_STOP].config.all=0;
										   rgb[LED_START_STOP].config.f.blink=1;
										   rgb[LED_START_STOP].red=0;
										   rgb[LED_START_STOP].green=100;
										   rgb[LED_START_STOP].blue=0;
										   set_led_level(LED_PROGRAM);
										}
						            }else{
						            	ionizer1.number_cycle=0;
						            	set_led(LED_UV,LED_OFF);
						            	ionizer1.task=STOP_CYCLE_TASK;
						            	printf(" STOP Cycle Ionize \n\r");
//						            	rgb[LED_START_STOP].config.f.refresh=1;
//										rgb[LED_START_STOP].red=100;
//									    rgb[LED_START_STOP].green=0;
//										rgb[LED_START_STOP].blue=0;
						            	set_led_level(LED_PROGRAM);
						            }
						         break;

		case(STOP_CYCLE_TASK):
				                 ionizer1.task=STOP_CYCLE_TASK;
		                         stop=read_sensor();
		                         keys.btn.bat_refresh=1;
		                         if(keys.btn.start_stop==BTN_NEG_RELEASED){//START/STOP button released//&&(L3G4200D_check_hysteresis()==true)
		                        	display_refresh_event=0;
				                	ionizer1.number_cycle=0;
									set_led(LED_UV,LED_OFF);
									ionizer1.task=STANDBY_TASK;
									rgb[LED_START_STOP].config.all=0;
									rgb[LED_START_STOP].config.f.fade=1;
								    rgb[LED_START_STOP].config.f.up_down=0;
								    rgb[LED_START_STOP].duty_fading=FADE_MIN_LEVEL;
				                 }

						         break;
		case(ERROR_TASK):
		                         ionizer1.task=ERROR_TASK;
		                         keys.btn.bat_refresh=1;
		                         rgb[LED_START_STOP].config.all=0;
								 rgb[LED_START_STOP].config.f.blink=1;
								 rgb[LED_START_STOP].red=100;
								 rgb[LED_START_STOP].green=0;
								 rgb[LED_START_STOP].blue=0;

				                 if(ionizer1.error.bytes==0){
				                	ionizer1.task=STOP_CYCLE_TASK;

				                 }
						         break;

		default:
			      break;
	}

	if(ionizer1.error.bytes!=0){
			      ionizer1.task=ERROR_TASK;
	}

	if(main_clock.flag_10ms==1){
		//L3G4200D_update();
		ADXL345_readActivites();
		if(ionizer1.error.bytes==0){
		 read_sensor();
		}
		if ( ADXL345_sens.act.isActivityOnZ) {
				printf("Activity On Z \n\r");
				printf("ACC_Z:%5.1f Old:%5.1f \n\r",ADXL345_sens.acc_z,ADXL345_sens.acc_z_old);
		}
		main_clock.flag_10ms=0;
	}

	if(main_clock.flag_100ms==1){
		//L3G4200D_update();
		ADXL345_getEvent();
		main_clock.flag_100ms=0;
	}


}



void ionize_process(void){


	set_led(LED_UV,LED_ON);
	//ionizer1.number_cycle=0;
    //ionizer1.direction =1;
    //while(end_process!=true){


}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
