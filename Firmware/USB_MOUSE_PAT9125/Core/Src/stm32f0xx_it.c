/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    stm32f0xx_it.c
  * @brief   Interrupt Service Routines.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_it.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "tim.h"
#include "globals.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN TD */
extern TIM_HandleTypeDef htim7;
extern UART_HandleTypeDef huart2;
extern TIM_HandleTypeDef htim1;
/* USER CODE END TD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/* External variables --------------------------------------------------------*/
extern PCD_HandleTypeDef hpcd_USB_FS;
extern TIM_HandleTypeDef htim7;
extern UART_HandleTypeDef huart2;
extern UART_HandleTypeDef huart3;
extern ADC_HandleTypeDef hadc;
/* USER CODE BEGIN EV */

/* USER CODE END EV */

/******************************************************************************/
/*           Cortex-M0 Processor Interruption and Exception Handlers          */
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void SVC_Handler(void)
{
  /* USER CODE BEGIN SVC_IRQn 0 */

  /* USER CODE END SVC_IRQn 0 */
  /* USER CODE BEGIN SVC_IRQn 1 */

  /* USER CODE END SVC_IRQn 1 */
}

/**
  * @brief This function handles Pendable request for system service.
  */
void PendSV_Handler(void)
{
  /* USER CODE BEGIN PendSV_IRQn 0 */

  /* USER CODE END PendSV_IRQn 0 */
  /* USER CODE BEGIN PendSV_IRQn 1 */

  /* USER CODE END PendSV_IRQn 1 */
}

/**
  * @brief This function handles System tick timer.
  */
void SysTick_Handler(void)
{
  /* USER CODE BEGIN SysTick_IRQn 0 */

  /* USER CODE END SysTick_IRQn 0 */
  HAL_IncTick();
  /* USER CODE BEGIN SysTick_IRQn 1 */

  /* USER CODE END SysTick_IRQn 1 */
}

/******************************************************************************/
/* STM32F0xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f0xx.s).                    */
/******************************************************************************/
/**
  * @brief This function handles ADC1, COMP1 and COMP2 interrupts (COMP interrupts through EXTI lines 17 and 18).
  */
void ADC1_COMP_IRQHandler(void)
{
  /* USER CODE BEGIN ADC1_COMP_IRQn 0 */

  /* USER CODE END ADC1_COMP_IRQn 0 */
  HAL_ADC_IRQHandler(&hadc);
  /* USER CODE BEGIN ADC1_COMP_IRQn 1 */

  /* USER CODE END ADC1_COMP_IRQn 1 */
}
/**
  * @brief This function handles TIM7 global interrupt.
  */
void TIM7_IRQHandler(void)
{
  /* USER CODE BEGIN TIM7_IRQn 0 */

  /* USER CODE END TIM7_IRQn 0 */
  HAL_TIM_IRQHandler(&htim7);
  /* USER CODE BEGIN TIM7_IRQn 1 */
  System_Clock_Timer_TickTIM7();
  /* USER CODE END TIM7_IRQn 1 */
}

/**
  * @brief This function handles USART3 and USART4 global interrupts.
  */
void USART3_4_IRQHandler(void)
{
  /* USER CODE BEGIN USART3_4_IRQn 0 */

  /* USER CODE END USART3_4_IRQn 0 */
  HAL_UART_IRQHandler(&huart3);
  /* USER CODE BEGIN USART3_4_IRQn 1 */

  /* USER CODE END USART3_4_IRQn 1 */
}

/**
  * @brief This function handles USB global interrupt / USB wake-up interrupt through EXTI line 18.
  */
void USB_IRQHandler(void)
{
  /* USER CODE BEGIN USB_IRQn 0 */

  /* USER CODE END USB_IRQn 0 */
  HAL_PCD_IRQHandler(&hpcd_USB_FS);
  /* USER CODE BEGIN USB_IRQn 1 */

  /* USER CODE END USB_IRQn 1 */
}

/* USER CODE BEGIN 1 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
	if(huart->Instance == USART1){


	}
	if(huart->Instance == USART2){


	}
}


/**
  * @brief  This function handles external line 4_15 interrupt request.
  * @param  None
  * @retval None
  */
void EXTI4_15_IRQHandler(void)
{
  HAL_GPIO_EXTI_IRQHandler(BTN_PROGRAM_Pin);
}


/* USER CODE BEGIN 1 */
void change_program(void){

	 if(keys.btn.docking_in==1){
		  /* Toggle LED2 *///START/STOP Button pressed
		 switch(ionizer1.task){
		 		case(STANDBY_TASK):
			        if(display_refresh_event==0){//NONE program active
						 if(++read_keyboard_event>3){
							read_keyboard_event=1;
						 }
						 ionizer1.number_cycle=0;
						 ionizer1.direction =1;
						 switch(read_keyboard_event){
						   case(0):
									//stepper1.speed=200;
									//stepper1.rotate=STEPS_IONIZE_AREA;;
									ionizer1.total_cycle=CYCLE_IONIZE_LOW;
						            ionizer1.total_ionize_period=PERIOD_IONIZE_LOW;
						            ionizer1.program_toggle_time=TOGGLE_TIME_LOW;
						            rgb[LED_PROGRAM].config.all=0;
								    rgb[LED_PROGRAM].config.f.refresh=1;
								    rgb[LED_PROGRAM].red=RED_LEV_0;
								    rgb[LED_PROGRAM].green=GREEN_LEV_0;
								    rgb[LED_PROGRAM].blue=BLUE_LEV_0;
									break;
						   case(1):
									//stepper1.speed=300;
									//stepper1.rotate=STEPS_IONIZE_AREA;
									ionizer1.total_cycle=CYCLE_IONIZE_LOW;
						            ionizer1.total_ionize_period=PERIOD_IONIZE_LOW;
						            ionizer1.program_toggle_time=TOGGLE_TIME_LOW;
						            rgb[LED_PROGRAM].config.all=0;
									rgb[LED_PROGRAM].config.f.refresh=1;
									rgb[LED_PROGRAM].red=RED_LEV_1;
									rgb[LED_PROGRAM].green=GREEN_LEV_1;
									rgb[LED_PROGRAM].blue=BLUE_LEV_1;
									break;
						   case(2):
									//stepper1.speed=500;
									//stepper1.rotate=STEPS_IONIZE_AREA;
									ionizer1.total_cycle=CYCLE_IONIZE_MEDIUM;
						            ionizer1.total_ionize_period=PERIOD_IONIZE_MEDIUM;
						            ionizer1.program_toggle_time=TOGGLE_TIME_MEDIUM;
						            rgb[LED_PROGRAM].config.all=0;
									rgb[LED_PROGRAM].config.f.refresh=1;
									rgb[LED_PROGRAM].red=RED_LEV_2;
									rgb[LED_PROGRAM].green=GREEN_LEV_2;
									rgb[LED_PROGRAM].blue=BLUE_LEV_2;
									break;
						   case(3):
									//stepper1.speed=1000;
									//stepper1.rotate=4096;
									ionizer1.total_cycle=CYCLE_IONIZE_HIGH;
						            ionizer1.total_ionize_period=PERIOD_IONIZE_HIGH;
						            ionizer1.program_toggle_time=TOGGLE_TIME_HIGH;
						            rgb[LED_PROGRAM].config.all=0;
									rgb[LED_PROGRAM].config.f.refresh=1;
									rgb[LED_PROGRAM].red=RED_LEV_3;
									rgb[LED_PROGRAM].green=GREEN_LEV_3;
									rgb[LED_PROGRAM].blue=BLUE_LEV_3;
									break;
						   default:

									break;
						 }
						 //display_refresh_event=1;
					 }
		 		     break;
		 		case(START_CYCLE_TASK):

		 		case(MOVEMENT_TASK):

		 		case(IONIZE_PROCESS_TASK):

		 		case(STOP_CYCLE_TASK):

		 		case(ERROR_TASK):

		 						         break;

		 		default:
		 			      break;
		 	}


	 }else{
		 ionizer1.number_cycle=0;
		 ionizer1.direction =1;
		 read_keyboard_event=0;
		ionizer1.total_cycle=CYCLE_IONIZE_LOW;
		ionizer1.total_ionize_period=PERIOD_IONIZE_LOW;
		ionizer1.program_toggle_time=TOGGLE_TIME_LOW;
		rgb[LED_PROGRAM].config.all=0;
		rgb[LED_PROGRAM].config.f.refresh=1;
		rgb[LED_PROGRAM].red=0;
		rgb[LED_PROGRAM].green=0;
		rgb[LED_PROGRAM].blue=0;
	 }
}
/**
  * @brief EXTI line detection callbacks
  * @param GPIO_Pin: Specifies the pins connected EXTI line
  * @retval None
  */

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if (GPIO_Pin == GPIO_PIN_13)
  {//BLUE Button CONFIG  Program pressed
	  change_program();
  }
}

/**
  * @brief  Conversion complete callback in non blocking mode
  * @param  hadc: ADC handle
  * @note   This example shows a simple way to report end of conversion
  *         and get conversion result. You can add your own implementation.
  * @retval None
  */
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef *hadc)
{
  /* Retrieve ADC conversion data */
  //uhADCxConvertedData = HAL_ADC_GetValue(hadc);

  /* Computation of ADC conversions raw data to physical values           */
  /* using helper macro.                                                  */
  //uhADCxConvertedData_Voltage_mVolt = __ADC_CALC_DATA_VOLTAGE(VDDA_APPLI, uhADCxConvertedData);

  /* Update status variable of ADC unitary conversion                     */
	adc_conversion_status = 1;


}

/* USER CODE END 1 */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
