#include "pat9130.h"

//#include <math.h>
#include "spi.h"
#include "iwdg.h"
#include "string.h"



static uint16_t read_reg(uint8_t reg);
static void write_reg(uint8_t reg, uint8_t _data);
static void write_ver_reg(uint8_t reg, uint8_t _data);
uint16_t pat9130_wr_reg_verify(uint8_t reg, uint16_t _data);



void pat9130_init() {
  //pat9130.addr = PAT9130_ADDR;
  //HAL_SPI_MspInit(&hspi2);//Wire.begin();
  pat9130.bitres12 = false;
  pat9130.x = 0;
  pat9130.y = 0;
  pat9130.x2 = 0;
  pat9130.y2 = 0;
  pat9130.b = 0;//Brightness
  pat9130.s = 0;//Shutter
  pat9130.iq = 0;

  printf("Reset PAT9130 \n\r");
  pat9130_reset();
  //pat9130.addr = read_reg(0x00);//PAT9130_ADDR;
  printf("ID PAT9130 ID1: 0x%2X\n\r",  pat9130.PID1); //Checks product ID to make sure communication protocol is working properly.
  printf("ID2: 0x%2X\n\r",  pat9130.PID2); //Checks product ID to make sure communication protocol is working properly.
     if( pat9130.PID1 != 0x31)
     {
         printf("Communication SPI protocol error! Terminating program.\n\r");
         //return;
     }
     else{

         //pat9130_set_res(240,240,true);
    	 //PAT9130 sensor recommended settings:


     }
}

void pat9130_reset() {
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);
  HAL_Delay(1);
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_RESET);
  HAL_Delay(1);
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);
  write_reg(PAT9130_CONFIG, 0x80); // Software reset (i.e. set bit7 to 1).
  HAL_Delay(2);//delay(1);
  pat9130_read_pid();
  HAL_Delay(1);

  write_reg(PAT9130_BANK_SELECTION, 0x00); // Switch to bank0, not allowed to perform OTS_RegWriteRead
//	write_reg(PAT9130_CONFIG, 0x97); // Software reset (i.e. set bit7 to 1).
	// It will reset to 0 automatically.
	// So perform OTS_RegWriteRead is not allowed.
//	HAL_Delay(2); // Delay 1ms
  write_reg(PAT9130_WP, 0x5A); // Disable Write Protect
//	write_ver_reg(PAT9130_RES_X, 0x0A); // Set X-axis resolution
//	write_ver_reg(PAT9130_RES_Y, 0x0A); // Set Y-axis resolution
//	write_reg(PAT9130_ORIENTATION, 0x04); // Set 12-bit X/Y data format
  write_ver_reg(PAT9130_LD_SRC, 0x02);// set LD current source to 6
  write_ver_reg(PAT9130_MFIO_CONF, 0x34);// set MFIO
//	// write_reg (0x4B, 0x04); // ONLY for VDD=VDDA=1.7~1.9V: for power saving
//	write_reg(0x20, 0x64);
//	write_reg(0x2B, 0x6D);
//	write_reg(0x32, 0x2F);
//	HAL_Delay(10); // Delay 10ms
//	write_reg(PAT9130_BANK_SELECTION,0x01); // Switch to bank1, not allowed to perform write_reg
//	write_reg(PAT9130_CONFIG,0x28);
//	write_reg(0x33,0xD0);
//	write_reg(0x36,0xC2);
//	write_reg(0x3E,0x01);
//	write_reg(0x3F,0x15);
//	write_reg(0x41,0x32);
//	write_reg(0x42,0x3B);
//	write_reg(0x43,0xF2);
//	write_reg(0x44,0x3B);
//	write_reg(0x45,0xF2);
//	write_reg(0x46,0x22);
//	write_reg(0x47,0x3B);
//	write_reg(0x48,0xF2);
//	write_reg(0x49,0x3B);
//	write_reg(0x4A,0xF0);
//	write_reg(0x58,0x98);
//	write_reg(0x59,0x0C);
//	write_reg(0x5A,0x08);
//	write_reg(0x5B,0x0C);
//	write_reg(0x5C,0x08);
//	write_reg(0x61,0x10);
//	write_reg(0x67,0x9B);
//	write_reg(0x6E,0x22);
//	write_reg(0x71,0x07);
//	write_reg(0x72,0x08);
//	write_reg(PAT9130_BANK_SELECTION, 0x00); // Switch to bank0, not allowed to perform write_reg
	write_reg(PAT9130_WP, 0x00); // Enable Write Protect



  //write_reg(PAT9130_CONFIG, 0x98);
  pat9130.yres = 15;//255;
  pat9130.xres = 15;//255;
  pat9130.bitres12=false;//true;
  pat9130_set_res(pat9130.yres,pat9130.xres,pat9130.bitres12);

  //delay(1);
  pat9130.x   = 0;
  pat9130.y   = 0;
  pat9130.x2  = 0;
  pat9130.y2  = 0;
  pat9130.b   = 0;
  pat9130.s   = 0;
}

bool pat9130_read_pid(){
  pat9130.PID1 = read_reg(PAT9130_PID1);//0x31 = 49dec
  pat9130.PID2 = read_reg(PAT9130_PID2);//0x91 = 145dex
  if ((pat9130.PID1 == 0x31) && (pat9130.PID2 == 0x61)) {
	  return true;
  }else{
	  return false;
  }
}

void pat9130_set_res(uint8_t xres, uint8_t yres, bool bitres12) {
//    ADDR,VALUE	    Init
//	{ 0x09, 0x5A },     //Disables write-protect.
//	{ 0x0D, 0xB5 },     //Sets X-axis resolution (necessary value here will depend on physical design and application).
//	{ 0x0E, 0xFF },     //Sets Y-axis resolution (necessary value here will depend on physical design and application).
//	{ 0x19, 0x04 },     //Sets 12-bit X/Y data format.
//	{ 0x4B, 0x04 },     //Needed for when using low-voltage rail (1.7-1.9V): Power saving configuration.

  if (bitres12) {
	 // write_ver_reg(PAT9130_ORIENTATION, 0x04);//12bit resolution
  }
  write_ver_reg(PAT9130_RES_X, xres);
  write_ver_reg(PAT9130_RES_Y, yres);

  //HAL_Delay(2); // Delay 1ms
  pat9130.yres = read_reg(PAT9130_RES_Y);
  pat9130.xres = read_reg(PAT9130_RES_X);
  pat9130.ld_scr=read_reg(PAT9130_LD_SRC);
  pat9130.mfio=read_reg(PAT9130_MFIO_CONF);
  //pat9130.bitres12 =  read_reg(PAT9130_ORIENTATION);

	printf("ResX:%dCPI",(pat9130.xres*50));
	printf("\t");
	printf("ResY:%dCPI",(pat9130.yres*50));
	printf("\t");
    printf("LaserCurr.:%d[mA]",pat9130.ld_scr);
    printf("\t");
    printf("MFIO:0x%2X",pat9130.mfio);
	printf("\n\r");
}

void pat9130_set_res_x(uint8_t xres) {
  write_reg(PAT9130_RES_X, xres);
  pat9130.xres = xres;
}

void pat9130_set_res_y(uint8_t yres) {
  write_reg(PAT9130_RES_Y, yres);
  pat9130.yres = yres;
}

bool pat9130_read_test(){
  
  pat9130.b = read_reg(PAT9130_FRAME);
  pat9130.s = read_reg(PAT9130_SHUTTER);
  
  if ((pat9130.s == -1) ||(pat9130.b == -1)){
  return true;
  }else{
	  return false;
  }
}


void pat9130_update() {
 int16_t mx=0,my=0;
 //uint16_t deltax=0,deltay;
 int16_t deltaX_l=0, deltaY_l=0;
 int16_t deltaX_h=0, deltaY_h=0;


  pat9130.ucMotion = read_reg(PAT9130_MOTION);

  //pat9130.gain=16;
  if ((pat9130.ucMotion & 0x80)&&(pat9130.ucMotion!=0xFF))
  {
	  //pat9130.ucXL = read_reg(PAT9130_DELTA_XL);

	 // pat9130.ucYL = read_reg(PAT9130_DELTA_YL);
	 // pat9130.ucXH = read_reg(PAT9130_DELTA_XH);
	 // pat9130.ucYH = read_reg(PAT9130_DELTA_YH);
	 // pat9130.dx =  pat9130.ucXL; //read_reg(PAT9130_DELTA_XL);
	 // pat9130.dy =  pat9130.ucYL;//read_reg(PAT9130_DELTA_YL);
	 // pat9130.iDX = pat9130.ucXL | ((pat9130.ucXH << 8) & 0xff00);
	 // pat9130.iDY = pat9130.ucYL | ((pat9130.ucYH << 8) & 0xff00);

     deltaX_l= read_reg(PAT9130_DELTA_XL);
     deltaY_l = (int16_t)read_reg(PAT9130_DELTA_YL);
     deltaX_h = ((int16_t)read_reg(PAT9130_DELTA_XH))<<8;
     deltaY_h = ((int16_t)read_reg(PAT9130_DELTA_YH))<<8;
     pat9130.iDX = deltaX_l | deltaX_h;
     pat9130.iDY = deltaY_l | deltaY_h;
   // if (pat9130.iDX & 0x800) pat9130.iDX -= 4096;
   // if (pat9130.iDY & 0x800) pat9130.iDY -= 4096;
     pat9130.x2 = (pat9130.iDX);
   	 pat9130.y2 = (pat9130.iDY);
	 if((pat9130.iDX >200)||(pat9130.iDX <-200)){
		  pat9130.iDX=0;
 	 }
	 if((pat9130.iDY >200)||(pat9130.iDY <-200)){
			  pat9130.iDY=0;
	 }
     pat9130.x = (pat9130.iDX);
	 pat9130.y = (pat9130.iDY);
	//pat9130.y2 += pat9130.dy;
	//pat9130.x2 += pat9130.dx;


    //pat9130.iDX=pat9130.iDX/2;
    //pat9130.iDY=pat9130.iDY/2;

    //pat9130.x =pat9130.x /256;
    //pat9130.y =pat9130.y /256;

    mx=-(pat9130.y);//inverted axis
    my=-(pat9130.x);
	if(mx>127){
	   mx=127;
	}else if(mx<-127){
	   mx=-127;
	}
	if(my>127){
	   my=127;
	}else if(my<-127){
	   my=-127;
	}

    mouse.x=(int8_t)mx;//(pat9130.dx*8);
    mouse.y=(int8_t)my;//(pat9130.dy*8);
    //pat9130.b = read_reg(PAT9130_FRAME);
    //pat9130.s = read_reg(PAT9130_SHUTTER);
    //pat9130.iq = read_reg(PAT9130_IQ);
    pat9130_printData();
    pat9130.counts++;
    if(pat9130.counts>1){
           //
           keys.btn.mouse_activity=1;
           pat9130.counts=0;
           pat9130.x=0;
           pat9130.y=0;
           pat9130.x2=0;
           pat9130.y2=0;
    }
  }else{
	  pat9130.dy=0;
	  pat9130.dx=0;
	  pat9130.iDX = 0;
	  pat9130.iDY =0;
	  //keys.btn.mouse_activity=0;
//	  if (pat9130.ucMotion==0xFF){
//		  printf("Error SPI\n\r");
//	  }
//	  else{
//		  pat9130.counts++;
//	  }
  }


//  deltax=abs(pat9130.iDX);
//  deltay=abs(pat9130.iDY);
//  if(deltax>20){
//    mx=(pat9130.iDX);
//  }else if(deltax>1) {////
//	       mx=0;//(pat9130.iDX);
//        }
//  if(deltay>20){
//    my=(pat9130.iDY);
//  }else if(deltay>1){////
//	      my=0;//(pat9130.iDY);
//        }
//  mx=-(pat9130.iDY);//inverted axis
//  my=-(pat9130.iDX);
//  mx=-(pat9130.y);//inverted axis
//  my=-(pat9130.x);
//  	  if(mx>127){
//  		 if(mx>200){
//  			 mx=0;
//  		 }else{
//  			 mx=127;
//  		 }
//  	  }else if(mx<-127){
//				if(mx<-200){
//					 mx=0;
//				 }else{
//					mx=-127;
//				 }
//  		    }
//
//  	  if(my>127){
//  		 if(my>200){
//			 my=0;
//		 }else{
//			 my=127;
//		 }
//  	    }else if(my<-127){
//  	    	     if(my<-200){
//					 my=0;
//				 }else{
//					my=-127;
//				 }
//  	  	    }

// mouse.x=(int8_t)mx;//(pat9130.dx*8);
// mouse.y=(int8_t)my;//(pat9130.dy*8);
// if(pat9130.counts>4){
//        pat9130_printData();
//        keys.btn.mouse_activity=1;
//        pat9130.counts=0;
//        pat9130.x=0;
//        pat9130.y=0;
//        pat9130.x2=0;
//        pat9130.y2=0;
//      }
  //pat9130_update_y2();
  //pat9130_update_x2();
}

void pat9130_update_y() {
	pat9130.ucMotion = read_reg(PAT9130_MOTION);
  if (pat9130.ucMotion & 0x80)
  {
	  pat9130.ucYL = read_reg(PAT9130_DELTA_YL);
	  pat9130.ucYH = read_reg(PAT9130_DELTA_YH);
	  pat9130.iDY = pat9130.ucYL | ((pat9130.ucYH << 8) & 0xff00);
      //if (pat9130.iDY & 0x800) pat9130.iDY -= 4096;
      pat9130.y -= pat9130.iDY;
  }
}

void pat9130_update_y2()
{
	pat9130.ucMotion = read_reg(PAT9130_MOTION);
  if (pat9130.ucMotion & 0x80) {
	  pat9130.dy = read_reg(PAT9130_DELTA_YL);
    pat9130.y2 -= pat9130.dy;
    
  }
}

void pat9130_update_x() {
	pat9130.ucMotion = read_reg(PAT9130_MOTION);
  if (pat9130.ucMotion & 0x80)
  {
	  pat9130.ucXL = read_reg(PAT9130_DELTA_XL);
	  pat9130.ucXH = read_reg(PAT9130_DELTA_XH);
	  pat9130.iDX = pat9130.ucXL | ((pat9130.ucXH << 8) & 0xff00);
      //if (pat9130.iDX & 0x800) pat9130.iDX -= 4096;
      pat9130.x += pat9130.iDX;
  }
}

void pat9130_update_x2()
{
	pat9130.ucMotion = read_reg(PAT9130_MOTION);
  if (pat9130.ucMotion & 0x80) {
	  pat9130.dx = read_reg(PAT9130_DELTA_XL);
    pat9130.x2 += pat9130.dx;
  }
}



//=========================================================================
void pat9130_grabData(void)
{

//	pat9130.deltaX_low = read_reg(PAT9130_DELTA_XL);        //Grabs data from the proper registers.
//	pat9130.deltaY_low = read_reg(PAT9130_DELTA_YL);
//	pat9130.deltaX_high = (read_reg(PAT9130_DELTA_XYH)<<4) & 0xF00;      //Grabs data and shifts it to make space to be combined with lower bits.
//	pat9130.deltaY_high = (read_reg(PAT9130_DELTA_XYH)<<8) & 0xF00;
//
//    if(pat9130.deltaX_high & 0x800)
//    	pat9130.deltaX_high |= 0xf000; // 12-bit data convert to 16-bit (two's comp)
//
//    if(pat9130.deltaY_high & 0x800)
//    	pat9130.deltaY_high |= 0xf000; // 12-bit data convert to 16-bit (2's comp)
//
//    pat9130.deltaX = pat9130.deltaX_high | pat9130.deltaX_low;      //Combined the low and high bits.
//    pat9130.deltaY = pat9130.deltaY_high | pat9130.deltaY_low;

    pat9130_update();
    //pat9130_update_y2();
    //pat9130_update_x2();
    //write_reg(PAT9130_CONFIG, 0x97); // Software reset (i.e. set bit7 to 1).

}


//=========================================================================
void pat9130_printData(void)
{
//    if(( pat9130.deltaX != 0) || ( pat9130.deltaY != 0))      //If there is deltaX or deltaY movement, print the data.
//    {
//    	 pat9130.totalX +=  pat9130.deltaX;
//    	 pat9130.totalY +=  pat9130.deltaY;
//
//        printf("deltaX: %d\t\t\tdeltaY: %d\n\r",  pat9130.deltaX,  pat9130.deltaY);    //Prints each individual count of deltaX and deltaY.
//        printf("X-axis Counts: %d\t\tY-axis Counts: %d\n\r",  pat9130.totalX,  pat9130.totalY);  //Prints the total movement made during runtime.
//    }
//
//    pat9130.deltaX = 0;                             //Resets deltaX and Y values to zero, otherwise previous data is stored until overwritten.
//    pat9130.deltaY = 0;
//    printf("Mot:0x%2X",pat9130.ucMotion);
	//printf("\t");
//	printf("bright:%d",pat9130.b);
//	printf("\t");
//	printf("shutt:0x%2X",pat9130.s);
//  printf("\t");
//    printf("imageQ:%d",pat9130.iq);
//	printf("\t");
	printf("iDX:%d",pat9130.iDX);
	printf("\t");
	printf("iDY:%d",pat9130.iDY);
	printf("\t");
//	printf("x2:%ld",pat9130.x2);
//	printf("\t");
//	printf("y2:%ld",pat9130.y2);
//	printf("\t");
	printf("mouseX:%d",mouse.x);
	printf("\t");
	printf("mouseY:%d",mouse.y);
	printf("\n\r");
}






//----PRIVATE----
static void write_reg(uint8_t reg, uint8_t _data) {
  uint8_t _reg = reg| 0x80;
  //Wire.begin();
//  Wire.beginTransmission(_addr);
//  Wire.write(_reg);
//  Wire.write(_data);
//  Wire.endTransmission();
  uint8_t pData[2];

  pData[0]=_reg;
  pData[1]=_data;

  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_RESET);

  HAL_SPI_Transmit(&hspi2, pData, 2, TIMEOUT_BYTE_SPI2);
  //HAL_SPI_Transmit(&hspi2, (pData+1), 1, TIMEOUT_BYTE_SPI2);
 // HAL_StatusTypeDef HAL_SPI_Receive(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
 // HAL_StatusTypeDef HAL_SPI_TransmitReceive(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size, uint32_t Timeout);
 // HAL_Delay(1);
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);


}
static void write_ver_reg(uint8_t reg, uint8_t _data) {
  uint8_t _reg = reg| 0x80;
  //Wire.begin();
//  Wire.beginTransmission(_addr);
//  Wire.write(_reg);
//  Wire.write(_data);
//  Wire.endTransmission();
  uint8_t pData[2],pRxData[2];//uint8_t pData[2];

  pData[0]=_reg;
  pData[1]=_data;
  pRxData[0]=0;
  //HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);
  do{

    HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_RESET);
    HAL_SPI_Transmit(&hspi2, pData, 2, TIMEOUT_BYTE_SPI2);
  //HAL_SPI_Transmit(&hspi2, (pData+1), 1, TIMEOUT_BYTE_SPI2);
 // HAL_StatusTypeDef HAL_SPI_Receive(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
 // HAL_StatusTypeDef HAL_SPI_TransmitReceive(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size, uint32_t Timeout);
    HAL_Delay(1);
    pRxData[0]=read_reg(reg);

  }while (pRxData[0]!=_data);

  printf("Reg:0x%2X",reg);
  printf("\t");
  printf("Val:%d",pRxData[0]);
  printf("\n\r");

}

static uint16_t read_reg(uint8_t reg) {
  uint8_t _reg = reg & 0x7F;
  uint8_t val2 = 4;
  uint8_t req_status;
//  Wire.beginTransmission(_addr);
//  Wire.write(_reg);
//  Wire.endTransmission();
//  Wire.requestFrom(_addr, val2);
//  uint8_t c =Wire.read();
//  Wire.endTransmission();
//  return c;
  uint8_t pData[2],pRxData[2];
  pData[0]=_reg ;                     //Set MSB to 0 to indicate read operation
  pData[1]=reg;
  pRxData[0]=val2;

  //HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_RESET);
  HAL_Delay(1);
  req_status=HAL_SPI_Transmit(&hspi2, pData, 1, TIMEOUT_BYTE_SPI2);
  //HAL_Delay(1);
  //HAL_SPI_Transmit(&hspi2, pData, 1, TIMEOUT_BYTE_SPI2);
  //HAL_Delay(1);
  req_status=HAL_SPI_Receive(&hspi2, pRxData, 1, TIMEOUT_BYTE_SPI2);
  if(req_status!= HAL_OK){//HAL_TIMEOUT
	  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);

	  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_RESET);

	  req_status=HAL_SPI_Transmit(&hspi2, pData, 1, TIMEOUT_BYTE_SPI2);
	  req_status=HAL_SPI_Receive(&hspi2, pRxData, 1, TIMEOUT_BYTE_SPI2);
	  if(req_status!= HAL_OK){
		   printf("Rd Fail\n\r");
	  }
  }
  //HAL_SPI_TransmitReceive(&hspi2,pData,pRxData,2,TIMEOUT_BYTE_SPI2);
  //HAL_Delay(1);//delay(1);
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);
  return pRxData[0];
}
