#include "pat9125.h"

//#include <math.h>
#include "spi.h"
#include "iwdg.h"
#include "string.h"



static uint16_t read_reg(uint8_t reg);
static void write_reg(uint8_t reg, uint8_t _data);
static void write_ver_reg(uint8_t reg, uint8_t _data);
uint16_t pat9125_wr_reg_verify(uint8_t reg, uint16_t _data);



void pat9125_init() {
  //pat9125.addr = PAT9125_ADDR;
  //HAL_SPI_MspInit(&hspi2);//Wire.begin();
  pat9125.bitres12 = false;
  pat9125.x = 0;
  pat9125.y = 0;
  pat9125.x2 = 0;
  pat9125.y2 = 0;
  pat9125.b = 0;//Brightness
  pat9125.s = 0;//Shutter

  printf("Reset PAT9125 \n\r");
  pat9125_reset();
  //pat9125.addr = read_reg(0x00);//PAT9125_ADDR;
  printf("ID PAT9125 ID1: 0x%2X\n\r",  pat9125.PID1); //Checks product ID to make sure communication protocol is working properly.
  printf("ID2: 0x%2X\n\r",  pat9125.PID2); //Checks product ID to make sure communication protocol is working properly.
     if( pat9125.PID1 != 0x31)
     {
         printf("Communication SPI protocol error! Terminating program.\n\r");
         //return;
     }
     else{
         //pat9125_set_res(240,240,true);
    	 //PAT9125 sensor recommended settings:
     }
}

void pat9125_reset() {
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);
  HAL_Delay(1);
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_RESET);
  HAL_Delay(1);
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);
  write_reg(PAT9125_CONFIG, 0x97); // Software reset (i.e. set bit7 to 1).
  HAL_Delay(3);//delay(1);
  pat9125_read_pid();
  HAL_Delay(1);

// write_reg(PAT9125_BANK_SELECTION, 0x00); // Switch to bank0, not allowed to perform OTS_RegWriteRead
//	write_reg(PAT9125_CONFIG, 0x97); // Software reset (i.e. set bit7 to 1).
	// It will reset to 0 automatically.
	// So perform OTS_RegWriteRead is not allowed.
//	HAL_Delay(2); // Delay 1ms
// write_reg(PAT9125_WP, 0x5A); // Disable Write Protect
//	write_ver_reg(PAT9125_RES_X, 0x0A); // Set X-axis resolution
//	write_ver_reg(PAT9125_RES_Y, 0x0A); // Set Y-axis resolution
//	write_reg(PAT9125_ORIENTATION, 0x04); // Set 12-bit X/Y data format
//	write_reg(0x5E, 0x08);
//	// write_reg (0x4B, 0x04); // ONLY for VDD=VDDA=1.7~1.9V: for power saving
//	write_reg(0x20, 0x64);
//	write_reg(0x2B, 0x6D);
//	write_reg(0x32, 0x2F);
//	HAL_Delay(10); // Delay 10ms
//	write_reg(PAT9125_BANK_SELECTION,0x01); // Switch to bank1, not allowed to perform write_reg
//	write_reg(PAT9125_CONFIG,0x28);
//	write_reg(0x33,0xD0);
//	write_reg(0x36,0xC2);
//	write_reg(0x3E,0x01);
//	write_reg(0x3F,0x15);
//	write_reg(0x41,0x32);
//	write_reg(0x42,0x3B);
//	write_reg(0x43,0xF2);
//	write_reg(0x44,0x3B);
//	write_reg(0x45,0xF2);
//	write_reg(0x46,0x22);
//	write_reg(0x47,0x3B);
//	write_reg(0x48,0xF2);
//	write_reg(0x49,0x3B);
//	write_reg(0x4A,0xF0);
//	write_reg(0x58,0x98);
//	write_reg(0x59,0x0C);
//	write_reg(0x5A,0x08);
//	write_reg(0x5B,0x0C);
//	write_reg(0x5C,0x08);
//	write_reg(0x61,0x10);
//	write_reg(0x67,0x9B);
//	write_reg(0x6E,0x22);
//	write_reg(0x71,0x07);
//	write_reg(0x72,0x08);
//	write_reg(PAT9125_BANK_SELECTION, 0x00); // Switch to bank0, not allowed to perform write_reg
//	write_reg(PAT9125_WP, 0x00); // Enable Write Protect

  //write_reg(PAT9125_CONFIG, 0x98);
  pat9125.yres = 254;//255;
  pat9125.xres = 254;//255;
  pat9125.bitres12=false;//true;
  pat9125_set_res(pat9125.yres,pat9125.xres,pat9125.bitres12);

  //delay(1);
  pat9125.x   = 0;
  pat9125.y   = 0;
  pat9125.x2  = 0;
  pat9125.y2  = 0;
  pat9125.b   = 0;
  pat9125.s   = 0;
}

bool pat9125_read_pid(){
  pat9125.PID1 = read_reg(PAT9125_PID1);//0x31 = 49dec
  pat9125.PID2 = read_reg(PAT9125_PID2);//0x91 = 145dex
  if ((pat9125.PID1 == 0x31) && (pat9125.PID2 == 0x91)) {
	  return true;
  }else{
	  return false;
  }
}

void pat9125_set_res(uint8_t xres, uint8_t yres, bool bitres12) {
//    ADDR,VALUE	    Init
//	{ 0x09, 0x5A },     //Disables write-protect.
//	{ 0x0D, 0xB5 },     //Sets X-axis resolution (necessary value here will depend on physical design and application).
//	{ 0x0E, 0xFF },     //Sets Y-axis resolution (necessary value here will depend on physical design and application).
//	{ 0x19, 0x04 },     //Sets 12-bit X/Y data format.
//	{ 0x4B, 0x04 },     //Needed for when using low-voltage rail (1.7-1.9V): Power saving configuration.

  if (bitres12) {
	  write_ver_reg(PAT9125_ORIENTATION, 0x04);//12bit resolution
  }
  write_ver_reg(PAT9125_RES_X, xres);
  write_ver_reg(PAT9125_RES_Y, yres);

  //HAL_Delay(2); // Delay 1ms
  pat9125.yres = read_reg(PAT9125_RES_Y);
  pat9125.xres = read_reg(PAT9125_RES_X);
  pat9125.bitres12 =  read_reg(PAT9125_ORIENTATION);

	printf("ResX:%dCPI",(pat9125.xres*5) );
	printf("\t");
	printf("ResY:%dCPI",(pat9125.yres*5));
	printf("\t");
    printf("12BitEn:%d",pat9125.bitres12);
	printf("\n\r");
}

void pat9125_set_res_x(uint8_t xres) {
  write_reg(PAT9125_RES_X, xres);
  pat9125.xres = xres;
}

void pat9125_set_res_y(uint8_t yres) {
  write_reg(PAT9125_RES_Y, yres);
  pat9125.yres = yres;
}

bool pat9125_read_test(){
  
  pat9125.b = read_reg(PAT9125_FRAME);
  pat9125.s = read_reg(PAT9125_SHUTTER);
  
  if ((pat9125.s == -1) ||(pat9125.b == -1)){
  return true;
  }else{
	  return false;
  }
}


void pat9125_update() {
 int16_t mx=0,my=0;
 //uint16_t deltax=0,deltay=0;

  pat9125.ucMotion = read_reg(PAT9125_MOTION);
  //pat9125.b = read_reg(PAT9125_FRAME);
  //pat9125.s = read_reg(PAT9125_SHUTTER);
  pat9125.gain=10;
  if ((pat9125.ucMotion & 0x80)&&(pat9125.ucMotion!=0xFF))
  {
	  pat9125.ucXL = read_reg(PAT9125_DELTA_XL);
	  pat9125.ucYL = read_reg(PAT9125_DELTA_YL);
	  pat9125.ucXYH = read_reg(PAT9125_DELTA_XYH);
	  pat9125.dx =  pat9125.ucXL; //read_reg(PAT9125_DELTA_XL);
	  pat9125.dy =  pat9125.ucYL;//read_reg(PAT9125_DELTA_YL);

	  pat9125.iDX = pat9125.ucXL | ((pat9125.ucXYH << 4) & 0xf00);
	  pat9125.iDY = pat9125.ucYL | ((pat9125.ucXYH << 8) & 0xf00);

    if (pat9125.iDX & 0x800) pat9125.iDX -= 4096;
    if (pat9125.iDY & 0x800) pat9125.iDY -= 4096;

      if((pat9125.iDX >200)||(pat9125.iDX <-200)){
  		  pat9125.iDX=0;
   	  }
  	  if((pat9125.iDY >200)||(pat9125.iDY <-200)){
  			  pat9125.iDY=0;
  	  }
      pat9125.x += ((pat9125.iDX)*pat9125.gain);
  	  pat9125.y += ((pat9125.iDY)*pat9125.gain);

  	  pat9125.counts++;
      mx=((pat9125.x)/pat9125.counts);//inverted axis
      my=((pat9125.y)/pat9125.counts);

	  if(mx>127){
		  mx=127;
	  }else if(mx<-127){
		  	  mx=-127;
		    }
	  if(my>127){
	  	  my=127;
	    }else if(my<-127){
	  	  	  my=-127;
	  	    }

		mouse.x=(int8_t)mx;//(pat9125.dx*8);
		mouse.y=(int8_t)my;//(pat9125.dy*8);


		if(pat9125.counts>0){
			  pat9125_printData();
			  keys.btn.mouse_activity=1;
			  pat9125.counts=0;
			  pat9125.x=0;
			  pat9125.y=0;
			  pat9125.x2=0;
			  pat9125.y2=0;
			}

  }else{
	  pat9125.dy=0;
	  pat9125.dx=0;
	  pat9125.iDX = 0;
	  pat9125.iDY =0;
	  //keys.btn.mouse_activity=0;
	  if (pat9125.ucMotion==0xFF){
		 // printf("Error SPI\n\r");
	  }
//	  else{
//		  pat9125.counts++;
//	  }
  }

//  deltax=abs(pat9125.x2);
//  deltay=abs(pat9125.y2);
//  if(deltax>4){
//    mx=(pat9125.x2*3);
//  }else if(deltax>1) {////
//	       mx=(pat9125.x2);
//        }
//  if(deltay>4){
//    my=(pat9125.y2*3);
//  }else if(deltay>1){////
//	      my=(pat9125.y2);
//        }
  //mx=pat9125.x2;
  //my=pat9125.y2;

  //pat9125_update_y2();
  //pat9125_update_x2();
}

void pat9125_update_y() {
	pat9125.ucMotion = read_reg(PAT9125_MOTION);
  if (pat9125.ucMotion & 0x80)
  {
	  pat9125.ucYL = read_reg(PAT9125_DELTA_YL);
	  pat9125.ucXYH = read_reg(PAT9125_DELTA_XYH);
	  pat9125.iDY = pat9125.ucYL | ((pat9125.ucXYH << 8) & 0xf00);
      if (pat9125.iDY & 0x800) {
    	pat9125.iDY -= 4096;
      }
      pat9125.y -= pat9125.iDY;
  }
}

void pat9125_update_y2()
{
	pat9125.ucMotion = read_reg(PAT9125_MOTION);
  if (pat9125.ucMotion & 0x80) {
	  pat9125.dy = read_reg(PAT9125_DELTA_YL);
    pat9125.y2 -= pat9125.dy;
    
  }
}

void pat9125_update_x() {
	pat9125.ucMotion = read_reg(PAT9125_MOTION);
  if (pat9125.ucMotion & 0x80)
  {
	  pat9125.ucXL = read_reg(PAT9125_DELTA_XL);
	  pat9125.ucXYH = read_reg(PAT9125_DELTA_XYH);
	  pat9125.iDX = pat9125.ucXL | ((pat9125.ucXYH << 4) & 0xf00);
      if (pat9125.iDX & 0x800) pat9125.iDX -= 4096;
      pat9125.x += pat9125.iDX;
  }
}

void pat9125_update_x2()
{
	pat9125.ucMotion = read_reg(PAT9125_MOTION);
  if (pat9125.ucMotion & 0x80) {
	  pat9125.dx = read_reg(PAT9125_DELTA_XL);
    pat9125.x2 += pat9125.dx;
  }
}



//=========================================================================
void pat9125_grabData(void)
{

//	pat9125.deltaX_low = read_reg(PAT9125_DELTA_XL);        //Grabs data from the proper registers.
//	pat9125.deltaY_low = read_reg(PAT9125_DELTA_YL);
//	pat9125.deltaX_high = (read_reg(PAT9125_DELTA_XYH)<<4) & 0xF00;      //Grabs data and shifts it to make space to be combined with lower bits.
//	pat9125.deltaY_high = (read_reg(PAT9125_DELTA_XYH)<<8) & 0xF00;
//
//    if(pat9125.deltaX_high & 0x800)
//    	pat9125.deltaX_high |= 0xf000; // 12-bit data convert to 16-bit (two's comp)
//
//    if(pat9125.deltaY_high & 0x800)
//    	pat9125.deltaY_high |= 0xf000; // 12-bit data convert to 16-bit (2's comp)
//
//    pat9125.deltaX = pat9125.deltaX_high | pat9125.deltaX_low;      //Combined the low and high bits.
//    pat9125.deltaY = pat9125.deltaY_high | pat9125.deltaY_low;

    pat9125_update();
    //pat9125_update_y2();
    //pat9125_update_x2();
    //write_reg(PAT9125_CONFIG, 0x97); // Software reset (i.e. set bit7 to 1).

}


//=========================================================================
void pat9125_printData(void)
{
//    if(( pat9125.deltaX != 0) || ( pat9125.deltaY != 0))      //If there is deltaX or deltaY movement, print the data.
//    {
//    	 pat9125.totalX +=  pat9125.deltaX;
//    	 pat9125.totalY +=  pat9125.deltaY;
//
//        printf("deltaX: %d\t\t\tdeltaY: %d\n\r",  pat9125.deltaX,  pat9125.deltaY);    //Prints each individual count of deltaX and deltaY.
//        printf("X-axis Counts: %d\t\tY-axis Counts: %d\n\r",  pat9125.totalX,  pat9125.totalY);  //Prints the total movement made during runtime.
//    }
//
//    pat9125.deltaX = 0;                             //Resets deltaX and Y values to zero, otherwise previous data is stored until overwritten.
//    pat9125.deltaY = 0;
//    printf("Mot:0x%2X",pat9125.ucMotion);
//    printf("\t");
//	printf("x:%ld",pat9125.x);
//	printf("\t");
//    printf("y:%ld",pat9125.y);
	//printf("\t");
	//printf("bright:%d",pat9125.b);
	//printf("\t");
	//printf("shutt:%d",pat9125.s);
	//printf("\t");
	//printf("x2:%ld",pat9125.x2);
	//printf("\t");
	//printf("y2:%ld",pat9125.y2);
 //   printf("\t");
  	printf("iDX:%d",pat9125.iDX);
  	printf("\t");
    printf("iDY:%d",pat9125.iDY);
	printf("\t");
	printf("mouseX:%d",mouse.x);
	printf("\t");
	printf("mouseY:%d",mouse.y);
	printf("\n\r");
}






//----PRIVATE----
static void write_reg(uint8_t reg, uint8_t _data) {
  uint8_t _reg = reg| 0x80;
  //Wire.begin();
//  Wire.beginTransmission(_addr);
//  Wire.write(_reg);
//  Wire.write(_data);
//  Wire.endTransmission();
  uint8_t pData[2];

  pData[0]=_reg;
  pData[1]=_data;

  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_RESET);

  HAL_SPI_Transmit(&hspi2, pData, 2, TIMEOUT_BYTE_SPI2);
  //HAL_SPI_Transmit(&hspi2, (pData+1), 1, TIMEOUT_BYTE_SPI2);
 // HAL_StatusTypeDef HAL_SPI_Receive(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
 // HAL_StatusTypeDef HAL_SPI_TransmitReceive(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size, uint32_t Timeout);
 // HAL_Delay(1);
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);


}
static void write_ver_reg(uint8_t reg, uint8_t _data) {
  uint8_t _reg = reg| 0x80;
  //Wire.begin();
//  Wire.beginTransmission(_addr);
//  Wire.write(_reg);
//  Wire.write(_data);
//  Wire.endTransmission();
  uint8_t pData[2],pRxData[2];//uint8_t pData[2];

  pData[0]=_reg;
  pData[1]=_data;
  pRxData[0]=0;
  //HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);
  do{

    HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_RESET);
    HAL_SPI_Transmit(&hspi2, pData, 2, TIMEOUT_BYTE_SPI2);
  //HAL_SPI_Transmit(&hspi2, (pData+1), 1, TIMEOUT_BYTE_SPI2);
 // HAL_StatusTypeDef HAL_SPI_Receive(SPI_HandleTypeDef *hspi, uint8_t *pData, uint16_t Size, uint32_t Timeout);
 // HAL_StatusTypeDef HAL_SPI_TransmitReceive(SPI_HandleTypeDef *hspi, uint8_t *pTxData, uint8_t *pRxData, uint16_t Size, uint32_t Timeout);
    HAL_Delay(1);
    pRxData[0]=read_reg(reg);

  }while (pRxData[0]!=_data);

  printf("Reg:0x%2X",reg);
  printf("\t");
  printf("Val:%d",pRxData[0]);
  printf("\n\r");

}

static uint16_t read_reg(uint8_t reg) {
  uint8_t _reg = reg & 0x7F;
  uint8_t val2 = 4;
  uint8_t req_status;
//  Wire.beginTransmission(_addr);
//  Wire.write(_reg);
//  Wire.endTransmission();
//  Wire.requestFrom(_addr, val2);
//  uint8_t c =Wire.read();
//  Wire.endTransmission();
//  return c;
  uint8_t pData[2],pRxData[2];
  pData[0]=_reg ;                     //Set MSB to 0 to indicate read operation
  pData[1]=reg;
  pRxData[0]=val2;

  //HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_RESET);
  HAL_Delay(1);
  req_status=HAL_SPI_Transmit(&hspi2, pData, 1, TIMEOUT_BYTE_SPI2);
  //HAL_Delay(1);
  //HAL_SPI_Transmit(&hspi2, pData, 1, TIMEOUT_BYTE_SPI2);
  //HAL_Delay(1);
  req_status=HAL_SPI_Receive(&hspi2, pRxData, 1, TIMEOUT_BYTE_SPI2);
  if(req_status!= HAL_OK){//HAL_TIMEOUT
	  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);

	  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_RESET);

	  req_status=HAL_SPI_Transmit(&hspi2, pData, 1, TIMEOUT_BYTE_SPI2);
	  req_status=HAL_SPI_Receive(&hspi2, pRxData, 1, TIMEOUT_BYTE_SPI2);
	  if(req_status!= HAL_OK){
		   printf("Rd Fail\n\r");
	  }
  }
  //HAL_SPI_TransmitReceive(&hspi2,pData,pRxData,2,TIMEOUT_BYTE_SPI2);
  //HAL_Delay(1);//delay(1);
  HAL_GPIO_WritePin(SPI2_NSS_GPIO_Port, SPI2_NSS_Pin, GPIO_PIN_SET);
  return pRxData[0];
}
